
package org.caebeans.caeserver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://caeserver.caebeans.org}instance"/>
 *         &lt;element ref="{http://caeserver.caebeans.org}problemCaebean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "instance",
    "problemCaebean"
})
@XmlRootElement(name = "submitJob")
public class SubmitJob {

    @XmlElement(required = true)
    protected Instance instance;
    @XmlElement(required = true)
    protected ProblemCaebean problemCaebean;

    /**
     * Gets the value of the instance property.
     * 
     * @return
     *     possible object is
     *     {@link Instance }
     *     
     */
    public Instance getInstance() {
        return instance;
    }

    /**
     * Sets the value of the instance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Instance }
     *     
     */
    public void setInstance(Instance value) {
        this.instance = value;
    }

    /**
     * Gets the value of the problemCaebean property.
     * 
     * @return
     *     possible object is
     *     {@link ProblemCaebean }
     *     
     */
    public ProblemCaebean getProblemCaebean() {
        return problemCaebean;
    }

    /**
     * Sets the value of the problemCaebean property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProblemCaebean }
     *     
     */
    public void setProblemCaebean(ProblemCaebean value) {
        this.problemCaebean = value;
    }

}
