
package org.caebeans.caeserver;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.caebeans.caeserver package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TerminationTime_QNAME = new QName("http://caeserver.caebeans.org", "TerminationTime");
    private final static QName _Data_QNAME = new QName("http://caeserver.caebeans.org", "data");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.caebeans.caeserver
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProblemCaebean }
     * 
     */
    public ProblemCaebean createProblemCaebean() {
        return new ProblemCaebean();
    }

    /**
     * Create an instance of {@link Parameter.Units }
     * 
     */
    public Parameter.Units createParameterUnits() {
        return new Parameter.Units();
    }

    /**
     * Create an instance of {@link CreateInstanceResponse }
     * 
     */
    public CreateInstanceResponse createCreateInstanceResponse() {
        return new CreateInstanceResponse();
    }

    /**
     * Create an instance of {@link Parameter }
     * 
     */
    public Parameter createParameter() {
        return new Parameter();
    }

    /**
     * Create an instance of {@link ProblemCaebean.Categories }
     * 
     */
    public ProblemCaebean.Categories createProblemCaebeanCategories() {
        return new ProblemCaebean.Categories();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link Parameter.Comment }
     * 
     */
    public Parameter.Comment createParameterComment() {
        return new Parameter.Comment();
    }

    /**
     * Create an instance of {@link CaeProject.Logical.Node }
     * 
     */
    public CaeProject.Logical.Node createCaeProjectLogicalNode() {
        return new CaeProject.Logical.Node();
    }

    /**
     * Create an instance of {@link CaeProject.Physical.ComponentCaebean.Input }
     * 
     */
    public CaeProject.Physical.ComponentCaebean.Input createCaeProjectPhysicalComponentCaebeanInput() {
        return new CaeProject.Physical.ComponentCaebean.Input();
    }

    /**
     * Create an instance of {@link GetStatusResponse }
     * 
     */
    public GetStatusResponse createGetStatusResponse() {
        return new GetStatusResponse();
    }

    /**
     * Create an instance of {@link ProblemCaebean.Resources }
     * 
     */
    public ProblemCaebean.Resources createProblemCaebeanResources() {
        return new ProblemCaebean.Resources();
    }

    /**
     * Create an instance of {@link CaeProject.Logical }
     * 
     */
    public CaeProject.Logical createCaeProjectLogical() {
        return new CaeProject.Logical();
    }

    /**
     * Create an instance of {@link CaeProject.Physical.ComponentCaebean }
     * 
     */
    public CaeProject.Physical.ComponentCaebean createCaeProjectPhysicalComponentCaebean() {
        return new CaeProject.Physical.ComponentCaebean();
    }

    /**
     * Create an instance of {@link CaeProject.Physical.ComponentCaebean.Exec }
     * 
     */
    public CaeProject.Physical.ComponentCaebean.Exec createCaeProjectPhysicalComponentCaebeanExec() {
        return new CaeProject.Physical.ComponentCaebean.Exec();
    }

    /**
     * Create an instance of {@link CaeProject.Physical.ComponentCaebean.JarPremode }
     * 
     */
    public CaeProject.Physical.ComponentCaebean.JarPremode createCaeProjectPhysicalComponentCaebeanJarPremode() {
        return new CaeProject.Physical.ComponentCaebean.JarPremode();
    }

    /**
     * Create an instance of {@link CaeProject.Physical }
     * 
     */
    public CaeProject.Physical createCaeProjectPhysical() {
        return new CaeProject.Physical();
    }

    /**
     * Create an instance of {@link CaeProject }
     * 
     */
    public CaeProject createCaeProject() {
        return new CaeProject();
    }

    /**
     * Create an instance of {@link Description }
     * 
     */
    public Description createDescription() {
        return new Description();
    }

    /**
     * Create an instance of {@link CaeProject.Logical.Edge }
     * 
     */
    public CaeProject.Logical.Edge createCaeProjectLogicalEdge() {
        return new CaeProject.Logical.Edge();
    }

    /**
     * Create an instance of {@link GetStatus }
     * 
     */
    public GetStatus createGetStatus() {
        return new GetStatus();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link CaeProject.Physical.ComponentCaebean.Exec.Param }
     * 
     */
    public CaeProject.Physical.ComponentCaebean.Exec.Param createCaeProjectPhysicalComponentCaebeanExecParam() {
        return new CaeProject.Physical.ComponentCaebean.Exec.Param();
    }

    /**
     * Create an instance of {@link CreateInstance }
     * 
     */
    public CreateInstance createCreateInstance() {
        return new CreateInstance();
    }

    /**
     * Create an instance of {@link SubmitJobResponse }
     * 
     */
    public SubmitJobResponse createSubmitJobResponse() {
        return new SubmitJobResponse();
    }

    /**
     * Create an instance of {@link ProblemCaebean.Categories.Category }
     * 
     */
    public ProblemCaebean.Categories.Category createProblemCaebeanCategoriesCategory() {
        return new ProblemCaebean.Categories.Category();
    }

    /**
     * Create an instance of {@link TerminationTime }
     * 
     */
    public TerminationTime createTerminationTime() {
        return new TerminationTime();
    }

    /**
     * Create an instance of {@link Instance }
     * 
     */
    public Instance createInstance() {
        return new Instance();
    }

    /**
     * Create an instance of {@link CaeProject.Physical.ComponentCaebean.Output }
     * 
     */
    public CaeProject.Physical.ComponentCaebean.Output createCaeProjectPhysicalComponentCaebeanOutput() {
        return new CaeProject.Physical.ComponentCaebean.Output();
    }

    /**
     * Create an instance of {@link CurrentTime }
     * 
     */
    public CurrentTime createCurrentTime() {
        return new CurrentTime();
    }

    /**
     * Create an instance of {@link ResourceData }
     * 
     */
    public ResourceData createLanguageData() {
        return new ResourceData();
    }

    /**
     * Create an instance of {@link SubmitJob }
     * 
     */
    public SubmitJob createSubmitJob() {
        return new SubmitJob();
    }

    /**
     * Create an instance of {@link Parameter.Text }
     * 
     */
    public Parameter.Text createParameterText() {
        return new Parameter.Text();
    }

    /**
     * Create an instance of {@link Resource }
     * 
     */
    public Resource createLanguage() {
        return new Resource();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminationTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://caeserver.caebeans.org", name = "TerminationTime")
    public JAXBElement<TerminationTime> createTerminationTime(TerminationTime value) {
        return new JAXBElement<TerminationTime>(_TerminationTime_QNAME, TerminationTime.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://caeserver.caebeans.org", name = "data")
    public JAXBElement<String> createData(String value) {
        return new JAXBElement<String>(_Data_QNAME, String.class, null, value);
    }

}
