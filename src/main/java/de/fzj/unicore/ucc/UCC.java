package de.fzj.unicore.ucc;

import de.fzj.unicore.ucc.helpers.DefaultMessageWriter;
import de.fzj.unicore.ucc.helpers.EndProcessingException;
import eu.unicore.security.util.Log;
import org.apache.commons.cli.ParseException;

import java.io.File;
import java.io.FileInputStream;
import java.text.NumberFormat;
import java.util.*;


/**
 * UCC main class and entry point
 * 
 * @author schuller
 */
public class UCC{

	public static boolean unitTesting = false;

	public static boolean mute = false;

	public static final Map<String, Class<? extends Command>> cmds = new HashMap<String, Class<? extends Command>>();

	public static final String UCC_EXTENSIONS = "ucc.extensions";

	public static Integer exitCode=null;

	public static NumberFormat numberFormat= NumberFormat.getInstance();
	
	private static MessageWriter msg=new DefaultMessageWriter();
	
	public static String getVersion(){
		String v=UCC.class.getPackage().getImplementationVersion();
		if(v==null)v="(DEVELOPMENT version)";
		return v;
	}
	
	static {
		ServiceLoader<ProvidedCommands> sl=ServiceLoader.load(ProvidedCommands.class);
		for(ProvidedCommands p: sl){
			Command[]commands=p.getCommands();
			for(Command cmd: commands){
				cmds.put(cmd.getName(), cmd.getClass());
			}
		}
		numberFormat.setMaximumFractionDigits(2);
	}

	public static void printUsage(boolean exit) {
		String version = getVersion();
		System.err.println("UCC " + version);
		System.err.println("Usage: ucc <command> [OPTIONS] <args>");
		System.err.println("The following commands are available:");
		List<Command> cmds=getAllCommands();
		Collections.sort(cmds, new Comparator<Command>() {
            public int compare(Command e1, Command e2) {
                return e1.getCommandGroup().compareTo(
                        e2.getCommandGroup());
            }
        });
		String lastGroup = "";
		for (Command entry : cmds) {
			String group = entry.getCommandGroup();
			if (!group.equalsIgnoreCase(lastGroup)) {
				System.err.println(group + ":");
				lastGroup = group;
			}
			System.err.printf(" %-20s - %s", entry.getName(), entry.getDescription());
			System.err.println();
		}

		System.err.println("Enter 'ucc <command> -h' for help on a particular command.");
		if (exit && !unitTesting){
			System.exit(1);
		}
	}

	public static void printUsage() {
		printUsage(true);
	}

	/**
	 * loads additional commands from the extentions file specified by
	 * <code>-Ducc.extensions=propertyfile</code>
	 */
	public static void loadUserActions() {
		String filename = System.getProperty(UCC_EXTENSIONS);
		try {
			File propFile = null;
			if (filename != null) {
				propFile = new File(filename);
				if (!propFile.exists()) {
					System.err.println("The extensions file <" + filename
							+ "> does not exist.");
					return;
				}
			} else {
				filename = System.getProperty("user.home") + File.separator
						+ ".ucc" + File.separator + "extensions";
				propFile = new File(filename);
				if (!propFile.exists()) {
					return;
				}
			}
			Properties p = new Properties();
			p.load(new FileInputStream(propFile));

			for (Object key : p.keySet()) {
				String cmdName = (String) key;
				String cmdClazz = p.getProperty(cmdName).trim();
				try {
					Class<?> cmdClass = Class.forName(cmdClazz);
					if (Command.class.isAssignableFrom(cmdClass)) {
						Command cmd = (Command) cmdClass.newInstance();
						cmds.put(cmdName, cmd.getClass());
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Could not load user action <" + key
							+ ">, please check the " + "extensions file <"
							+ filename + ">.");
				}
			}
		} catch (Exception e) {
			System.err.println("Could not load user actions, please check the "
					+ "extensions file <" + filename + ">.");
		}
	}

	/**
	 * creates and initialises a UCC {@link Command} class
	 * 
	 * @param args - the arguments for the command, where the first argument is the command name
	 * @param shouldQuit - whether UCC should exit if no command could be found
	 *           
	 * @return an initialised {@link Command} object 
	 */
	public static Command initCommand(String[] args, boolean shouldQuit)
			throws ParseException {
		String command = args[0];
		Class<? extends Command> cmdClass = cmds.get(command);
		if (cmdClass == null) {
			printUsage(shouldQuit);
			return null;
		}
		Command cmd=getCommand(command);
		
		try{
			cmd.init(args);
		} catch (ParseException pe) {
			cmd.printUsage();
			throw pe;
		}
		return cmd;
	}


	public static Command getCommand(String name){
		Class<? extends Command> cmdClass = cmds.get(name);
		if(cmdClass==null)throw new IllegalArgumentException("No such command: "+name);
		try{
			return cmdClass.newInstance();
		}catch(Exception ex){
			throw new RuntimeException("Could not instantiate "+cmdClass.getName(),ex);
		}
	}
	
	public static List<Command> getAllCommands(){
		List<Command> result=new ArrayList<Command>();
		for(Class<? extends Command> c: cmds.values()){
			try{
				result.add(c.newInstance());
			}catch(Exception ex){
				msg.error("Could not instantiate "+c.getName(), ex);
			}
		}
		return result;
	}
	
	/**
	 * Parses commandline args, initiates and runs action
	 */
	public static void main(String[] args) {
		UCC.loadUserActions();
		Command cmd = null;
		exitCode = null;
		try {
			if (args.length < 1 || args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("-h")) {
				printUsage(!unitTesting);
			}else{
				cmd = initCommand(args, !unitTesting);
				cmd.process();
				cmd.postProcess();
			}
			exitCode=0;
		} catch (EndProcessingException epe) {
			exitCode = epe.getExitCode();
		} catch (ParseException pe) {
			System.err.println("Error parsing commandline");
			pe.printStackTrace();
			exitCode = Constants.ERROR;
		} catch (Exception e) {
			if (cmd == null) {
				String msg=Log.createFaultMessage("Error setting up UCC command '"+args[0]+"'", e);
				System.err.println(msg);
			} else {
				cmd.error("An error occurred.", e);
			}
			exitCode = Constants.ERROR;
		}

		if (!unitTesting){
			System.exit(exitCode);
		}
		
	}

	/**
	 * get the current default message writer
	 */
	public static MessageWriter getMessageWriter(){
		return msg;
	}
	
	/**
	 * set the current default message writer
	 * @param writer
	 */
	public static void setMessageWriter(MessageWriter writer){
		msg=writer;
	}

}
