package de.fzj.unicore.ucc.utils;

import de.fzj.unicore.uas.client.StorageClient;
import de.fzj.unicore.uas.util.PropertyHelper;
import de.fzj.unicore.ucc.MessageWriter;
import org.unigrids.services.atomic.types.GridFileType;
import org.unigrids.services.atomic.types.ProtocolType;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;
import java.util.regex.Pattern;

public class FileTransferBase {

	protected Properties extraParameterSource;

	protected boolean timing=false;

	protected boolean recurse=false;

	/**
	 * the local file name
	 */
	protected String from;

	/**
	 * the path of the target in USpace
	 */
	protected String to;

	/**
	 * the creation mode
	 */
	protected Mode mode;

	/**
	 * whether the job processing should fail if an error occurs
	 */
	protected boolean failOnError;

	protected List<ProtocolType.Enum> preferredProtocols=new ArrayList<ProtocolType.Enum>();

	public FileTransferBase(){
		preferredProtocols.add(ProtocolType.BFT);
	}

	protected Map<String,String> makeExtraParameters(ProtocolType.Enum protocol, MessageWriter msg){
		Map<String, String> res;
		if(extraParameterSource==null){
			res=new HashMap<String, String>();
		}
		else{
			String p= String.valueOf(protocol);
			PropertyHelper ph=new PropertyHelper(extraParameterSource, new String[]{p,p.toLowerCase()});
			res= ph.getFilteredMap();
		}
		if(res.size()>0){
			msg.verbose("Have "+res.size()+" extra parameters for protocol "+protocol);
		}
		return res;
	}
	
	
	public String getTo() {
		return to;
	}

	public String getFrom() {
		return from;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Mode getMode() {
		return mode;
	}

	public boolean isFailOnError() {
		return failOnError;
	}

	public boolean isTiming() {
		return timing;
	}

	public void setTiming(boolean timing) {
		this.timing = timing;
	}

	public void setFailOnError(boolean failOnError) {
		this.failOnError = failOnError;
	}

	public List<ProtocolType.Enum> getPreferredProtocols() {
		return preferredProtocols;
	}

	public void setPreferredProtocols(List<ProtocolType.Enum> preferredProtocols) {
		this.preferredProtocols = preferredProtocols;
	}

	public void setExtraParameterSource(Properties properties){
		this.extraParameterSource=properties;
	}

	public void setRecurse(boolean recurse) {
		this.recurse = recurse;
	}
	/**
	 * check if the given path denotes a valid remote directory
	 * @param remotePath - the path
	 * @param sms - the storage
	 * @return
	 */
	protected boolean isValidDirectory(String remotePath, StorageClient sms){
		boolean result=false;
		if(! ("/".equals(remotePath) || ".".equals(remotePath)) ){
			try{
				GridFileType gft=sms.listProperties(remotePath);
				result=gft.getIsDirectory();
			}catch(Exception ex){
				result=false;
			}
		}
		else result=true;
		
		return result;
	}
	
	public File[] resolveWildCards(File original){
		final String name=original.getName();
		if(!hasWildCards(original))return new File[]{original};
		File parent=original.getParentFile();
		if(parent==null)parent=new File(".");
		FilenameFilter filter=new FilenameFilter(){
			Pattern p=createPattern(name);
			public boolean accept(File file, String name){
				return p.matcher(name).matches();
			}
		};
		return parent.listFiles(filter);
	}

	protected boolean hasWildCards(File file){
		return hasWildCards(file.getName());
	}

	public boolean hasWildCards(String name){
		return name.contains("*") || name.contains("?");
	}

	private Pattern createPattern(String nameWithWildcards){
		String regex=nameWithWildcards.replace("?",".").replace("*", ".*");
		return Pattern.compile(regex);
	}
	
	protected ProtocolType.Enum chosenProtocol=null;
	
	public ProtocolType.Enum getChosenProtocol(){
		return chosenProtocol;
	}
}
