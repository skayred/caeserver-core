package de.fzj.unicore.ucc.utils;

import de.fzj.unicore.ucc.MessageWriter;
import de.fzj.unicore.wsrflite.xmlbeans.client.IRegistryQuery;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import de.fzj.unicore.wsrflite.xmlbeans.client.Resources;
import org.oasisOpen.docs.wsrf.sg2.EntryType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * a more flexible version of a multi-registry client where the
 * individual registries need not be UNICORE6 RegistryClient instances
 * 
 * This is read-only, i.e. you cannot add to the registries using this 
 * client
 * 
 * @schuller
 */
public class MultiRegistryQuery implements IRegistryQuery {

	private final List<IRegistryQuery> clients=new ArrayList<IRegistryQuery>();
	
	private boolean filterDuplicates=true;
	
	private String connectionStatus=null;

	private final MessageWriter msg;
	
	public MultiRegistryQuery(MessageWriter msg){
		this.msg=msg;
	}
	
	public void addRegistry(IRegistryQuery registry){
		clients.add(registry);
	}
	
	public List<EndpointReferenceType> listAccessibleServices(QName porttype) throws Exception {
		List<EndpointReferenceType> result=new ArrayList<EndpointReferenceType>();
		for(IRegistryQuery c: clients){
			if(!c.checkConnection())continue;
			List<EndpointReferenceType> res=c.listAccessibleServices(porttype);
			if(filterDuplicates){
				addIfNotExist(result, res);
			}else{
				result.addAll(res);
			}
		}
		return result;
	}
	
	public List<EndpointReferenceType> listServices(QName porttype, ServiceListFilter acceptFilter) throws Exception {
		List<EndpointReferenceType> result=new ArrayList<EndpointReferenceType>();
		for(IRegistryQuery c: clients){
			if(!c.checkConnection())continue;
			List<EndpointReferenceType> res=c.listServices(porttype,acceptFilter);
			if(filterDuplicates){
				addIfNotExist(result, res);
			}else{
				result.addAll(res);
			}
		}
		return result;
	}

	public List<EndpointReferenceType> listServices(QName porttype) throws Exception {
		List<EndpointReferenceType> result=new ArrayList<EndpointReferenceType>();
		for(IRegistryQuery c: clients){
			if(!c.checkConnection())continue;
			List<EndpointReferenceType> res=c.listServices(porttype);
			if(filterDuplicates){
				addIfNotExist(result, res);
			}else{
				result.addAll(res);
			}
		}
		return result;
	}
	
	/**
	 * List all the entries in all the registries. No duplicate filtering
	 * is applied
	 */
	public List<EntryType> listEntries() {
		List<EntryType> result=new ArrayList<EntryType>();
		for(IRegistryQuery c: clients){
			try{
				result.addAll(c.listEntries());
			}catch(Exception ex){
				msg.verbose("Registry at "+getAddress(c)+" is not available: "+ex.getMessage());
			}
		}
		return result;
	}

	public String getConnectionStatus(){
		checkConnection();
		return connectionStatus;		
	}

	private void addIfNotExist(List<EndpointReferenceType> target, List<EndpointReferenceType> source){
		Set<String> addresses=new HashSet<String>();
		for(EndpointReferenceType epr: target){
			addresses.add(epr.getAddress().getStringValue());
		}
		for(EndpointReferenceType e: source){
			String address=e.getAddress().getStringValue();
			if(!addresses.contains(address)){
				addresses.add(address);
				target.add(e);
			}
		}
	}

	public boolean isFilterDuplicates() {
		return filterDuplicates;
	}

	public void setFilterDuplicates(boolean filterDuplicates) {
		this.filterDuplicates = filterDuplicates;
	}
	
	private Boolean compute(Callable<Boolean> task, int timeout){
		try{
			Future<Boolean> f=Resources.getExecutorService().submit(task);
			return f.get(timeout, TimeUnit.MILLISECONDS);
		}catch(Exception ex){
			return Boolean.FALSE;
		}
	}
	
	/**
	 * check the connection to the services. If the service does
	 * not reply within the given timeout, returns <code>false</code>
	 * @param timeout - connection timeout in milliseconds
	 * @return
	 */
	public boolean checkConnection(){
		return checkConnection(2000);
	}
	
	/**
	 * check the connection to the services. If no service 
	 * replies within the given timeout, returns <code>false</code>
	 * 
	 * @param timeout - connection timeout in milliseconds
	 * @return
	 */
	public boolean checkConnection(int timeout){
		final StringBuffer status=new StringBuffer();
		boolean result=false;
		for(final IRegistryQuery c: clients){
			Callable<Boolean> task=new Callable<Boolean>(){
				public Boolean call()throws Exception {
					return Boolean.valueOf(c.checkConnection());
				}
			};
			
			Boolean res=compute(task, timeout);
			boolean currentOK=res!=null?res.booleanValue():false;
			if(!currentOK){
				status.append("[NOT AVAILABLE: ").append(getAddress(c));
				status.append("] ");
			}
			result=result || currentOK;
			
		}
		if(result)connectionStatus="OK";
		else connectionStatus=status.toString();
		
		return result;
	}
	
	String getAddress(IRegistryQuery c){
		if(c instanceof RegistryClient){
			return ((RegistryClient)c).getUrl();
		}
		else{
			//some non U6-Registry
			return c.getClass().getName();
		}
	}
	
}
