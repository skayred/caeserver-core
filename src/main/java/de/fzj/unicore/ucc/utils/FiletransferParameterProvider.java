package de.fzj.unicore.ucc.utils;

import de.fzj.unicore.uas.client.UFTPConstants;
import de.fzj.unicore.ucc.MessageWriter;
import de.fzj.unicore.ucc.UCC;

import java.net.InetAddress;
import java.util.Map;
import java.util.UUID;

public class FiletransferParameterProvider implements
		de.fzj.unicore.uas.FiletransferParameterProvider {

	@Override
	public void provideParameters(Map<String, String> params, String protocol) {
		MessageWriter msg= UCC.getMessageWriter();
		
		if("UFTP".equals(protocol)){
			//check hostname
			String host=params.get(UFTPConstants.PARAM_CLIENT_HOST);
			if(host==null){
				try{
					host= InetAddress.getLocalHost().getCanonicalHostName();
					msg.verbose("UFTP: parameter <"+UFTPConstants.PARAM_CLIENT_HOST
							+"> not set, using <"+host+">");
					params.put(UFTPConstants.PARAM_CLIENT_HOST,host);
				}
				catch(Exception e){
					msg.error("Can't resolve name of local host", e);
				}
			}
			
			//number of streams
			String streams=params.get(UFTPConstants.PARAM_STREAMS);
			if(streams==null){
				msg.verbose("UFTP: parameter <"+UFTPConstants.PARAM_STREAMS+"> is not set, will use default value of <2>");
				params.put(UFTPConstants.PARAM_STREAMS,"2");
			}
			
			//secret -- must be a unique string
			params.put(UFTPConstants.PARAM_SECRET, UUID.randomUUID().toString());
			
		}
	}
	
}
