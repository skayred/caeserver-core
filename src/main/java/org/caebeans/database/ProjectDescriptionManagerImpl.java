package org.caebeans.database;

import com.google.inject.Inject;
import org.caebeans.lib.RandomGenerator;
import org.caebeans.models.CAESessionFactory;
import org.caebeans.models.Description;
import org.caebeans.task.description.TaskDescription;
import org.hibernate.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/18/12
 * Time: 7:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProjectDescriptionManagerImpl implements ProjectDescriptionManager {
    private SessionFactory sessionFactory;

    @Inject
    ProjectDescriptionManagerImpl() {
        sessionFactory = CAESessionFactory.getSessionFactory();
    }

    @Override
    public void loadDescription(TaskDescription description) {
        Session session = sessionFactory.openSession();
        String projectId = description.getProjectId();

        Transaction transaction = null;

        try {
            Description desc = findWork(projectId);
            transaction = session.beginTransaction();

            if (desc == null) {
                desc = new Description();
                desc.setProjectId(projectId);
                desc.setDescription(description);
                session.save(desc);
            } else {
                System.out.println("Description is not null");

                desc.setDescription(description);
                session.update(desc);
            }

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) { transaction.rollback(); }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public TaskDescription getDescription(String projectId) throws ClassNotFoundException, IOException {
        return findWork(projectId).getDescription();
    }

    @Override
    public List<TaskDescription> getAllDescriptions() throws ClassNotFoundException, IOException {
        List<TaskDescription> result = new ArrayList<TaskDescription>();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List employees = session.createSQLQuery("SELECT * FROM ProjectDescription").addEntity(Description.class).list();
            for (Iterator iterator = employees.iterator(); iterator.hasNext(); ) {
                Description description = (Description) iterator.next();

                result.add(description.getDescription());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            session.close();
        }

        return result;
    }

    @Override
    public List<String> getProjectsIDs() throws IOException, ClassNotFoundException {
        List<TaskDescription> descriptions = getAllDescriptions();
        List<String> ids = new ArrayList<String>();

        for (TaskDescription description : descriptions) {
            ids.add(description.getProjectId());
        }

        return ids;
    }

    @Override
    public String generateProjectID() throws IOException, ClassNotFoundException {
        List<String> ids = getProjectsIDs();
        String ID = RandomGenerator.generateUniqueID();

        while (ids.contains(ID)) {
            ID = RandomGenerator.generateUniqueID();
        }

        return ID;
    }

    @Override
    public void deleteAllDescriptions() {
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        Query deleteQuery = session.createSQLQuery("DELETE FROM ProjectDescription").addEntity(Description.class);
        deleteQuery.executeUpdate();
        session.getTransaction().commit();
    }

    private Description findWork(String projectId) {
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        Description description = (Description) session.get(Description.class, projectId);
        session.getTransaction().commit();

        return description;
    }
}
