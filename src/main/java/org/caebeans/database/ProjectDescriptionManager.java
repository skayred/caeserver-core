package org.caebeans.database;

import org.caebeans.caeserver.ProblemCaebean;
import org.caebeans.task.builder.TaskBuilder;
import org.caebeans.task.description.TaskDescription;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/18/12
 * Time: 7:22 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ProjectDescriptionManager {
    void loadDescription(TaskDescription description);

    TaskDescription getDescription(String projectId) throws ClassNotFoundException, IOException;

    List<TaskDescription> getAllDescriptions() throws ClassNotFoundException, IOException;

    List<String> getProjectsIDs() throws IOException, ClassNotFoundException;

    String generateProjectID() throws IOException, ClassNotFoundException;

    void deleteAllDescriptions();
}
