package org.caebeans.database;

import org.caebeans.models.Work;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/10/12
 * Time: 1:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface WorkManager {
    void createWork(String instanceId, String projectID, int memoryVolume, int cpuCount);

    void startWork(String instanceId);

    void finishWork(String instanceId);

    void failWork(String instanceId);

    void workTimedOut(String instanceId);

    int getStatus(String instanceID);

    long getExecutionTime(String instanceID);

    List<Work> getUnfinishedWorks();
}
