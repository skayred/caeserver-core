package org.caebeans.database;

import com.google.inject.Inject;
import org.caebeans.lib.LoggerManager;
import org.caebeans.models.CAESessionFactory;
import org.caebeans.models.Work;
import org.caebeans.storage.ResultStorageManager;
import org.caebeans.storage.WorkStorageManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/10/12
 * Time: 1:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class WorkManagerImpl implements WorkManager {
    private SessionFactory sessionFactory;

    @Inject    private LoggerManager logger;
    @Inject    private WorkStorageManager workStorageManager;
    @Inject    private ResultStorageManager resultStorageManager;

    @Inject
    WorkManagerImpl() {
        sessionFactory = CAESessionFactory.getSessionFactory();
    }

    @Override
    public void createWork(String instanceId, String projectID, int memoryVolume, int cpuCount) {
        Session session = sessionFactory.openSession();

        Transaction transaction = null;

        try {
            Work work = findWork(instanceId);

            transaction = session.beginTransaction();

            if (work == null) {
                work = new Work();
                work.setInstanceId(instanceId);
                work.setProjectID(projectID);
                work.setMemory(memoryVolume);
                work.setCpuCount(cpuCount);
            }
            work.setStatus(Work.STATUS_UNSTARTED);
            session.save(work);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null)
                transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void startWork(String instanceId) {
        setStatus(instanceId, Work.STATUS_STARTED);
    }

    @Override
    public void finishWork(String instanceID) {
        try {
            resultStorageManager.getResultPackage(instanceID);
        } catch (IOException e) {
            logger.fatal("Unable to pack results to ZIP", e);
        }

        setStatus(instanceID, Work.STATUS_FINISHED);
    }

    @Override
    public void failWork(String instanceID) {
        try {
            workStorageManager.getWorkPackage(instanceID);
        } catch (IOException e) {
            logger.fatal("Unable to pack work to ZIP", e);
        }

        setStatus(instanceID, Work.STATUS_FAILED);
    }

    @Override
    public void workTimedOut(String instanceID) {
        try {
            workStorageManager.getWorkPackage(instanceID);
        } catch (IOException e) {
            logger.fatal("Unable to pack work to ZIP", e);
        }

        setStatus(instanceID, Work.STATUS_TIME_LIMIT);
    }

    @Override
    public int getStatus(String instanceID) {
        Work work = findWork(instanceID);

        if (work != null) {
            return work.getStatus();
        } else {
            return Work.STATUS_UNSTARTED;
        }
    }

    @Override
    public long getExecutionTime(String instanceID) {
        Work work = findWork(instanceID);

        if (work != null) {
            return work.getEndTime() - work.getStartTime();
        }

        return -1;
    }

    @Override
    public List<Work> getUnfinishedWorks() {
        Session session = sessionFactory.openSession();

        List<Work> unfinishedWorks = new ArrayList<Work>();
        List<Work> works = (List<Work>) session.createQuery("from Work as work").list();

        for (Work work : works) {
            if (work.getStatus() == Work.STATUS_STARTED) {
                logger.warn("Job " + work.getInstanceId() + " unfinished!");
                unfinishedWorks.add(work);
            }
        }

        logger.warn("Found " + unfinishedWorks.size() + " unfinished works");

        session.close();

        return unfinishedWorks;
    }

    private Work findWork(String instanceId) {
        Session session = sessionFactory.openSession();

        session.beginTransaction();
        Work work = (Work) session.get(Work.class, instanceId);
        session.getTransaction().commit();

        session.close();

        return work;
    }

    private void setStatus(String instanceId, int status) {
        Session session = sessionFactory.openSession();

        Transaction transaction = null;

        try {
            Work work = findWork(instanceId);

            transaction = session.beginTransaction();
            work.setStatus(status);

            if (status == Work.STATUS_STARTED) {
                work.setStartTime(System.currentTimeMillis());
            }

            if ((status == Work.STATUS_FAILED) || (status == Work.STATUS_FINISHED)) {
                work.setEndTime(System.currentTimeMillis());
            }

            session.update(work);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null)
                transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
