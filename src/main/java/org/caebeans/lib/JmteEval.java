package org.caebeans.lib;

import com.floreysoft.jmte.Engine;

import javax.script.ScriptException;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/23/12
 * Time: 4:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class JmteEval {
    private Map<String, Object> model;

    public JmteEval(Map<String, Param> params) {
        model = new HashMap<String, Object>();

        for (Map.Entry<String, Param> param : params.entrySet()) {
            model.put(param.getKey(), param.getValue().getValue());
        }
    }

    public void eval(File input, File output) throws ScriptException, IOException {
        String inputContent = readFile(input);

        Engine engine = new Engine();
        engine.registerNamedRenderer(new FormatRenderer());
        String transformed = engine.transform(inputContent, model);

        BufferedWriter out = new BufferedWriter(new FileWriter(output));
        out.write(transformed);
        out.close();
    }

    private String readFile(File file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");

        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }

        return stringBuilder.toString();
    }
}
