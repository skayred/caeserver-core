package org.caebeans.lib;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 02.07.12
 * Time: 19:59
 * To change this template use File | Settings | File Templates.
 */
public interface ParamsParser {
    Map<String, Param> getParamsHash(String projectID, String instanceID) throws ParserConfigurationException, IOException, SAXException;
}
