package org.caebeans.lib;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 1/4/13
 * Time: 8:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class MapHelpers {
    public static <T, K> Map<T, K> merge(Map<T, K> first, Map<T, K> second) {
        Map<T, K> results = new HashMap<T, K>();

        for (Map.Entry<T, K> firstEntry : first.entrySet()) {
            results.put(firstEntry.getKey(), firstEntry.getValue());
        }

        for (Map.Entry<T, K> secondEntry : second.entrySet()) {
            results.put(secondEntry.getKey(), secondEntry.getValue());
        }

        return results;
    }
}
