package org.caebeans.lib;

import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/7/12
 * Time: 8:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class InstancesImpl implements Instances {
    private List<String> instances;

    @Inject
    InstancesImpl() {
        instances = new ArrayList<String>();
    }

    @Override
    public int size() {
        return instances.size();
    }

    @Override
    public boolean contains(String id) {
        return instances.contains(id);
    }

    @Override
    public void add(String id) {
        instances.add(id);
    }

    @Override
    public String toString() {
        return instances.toString();
    }
}
