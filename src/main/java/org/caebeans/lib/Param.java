package org.caebeans.lib;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 02.07.12
 * Time: 20:11
 * To change this template use File | Settings | File Templates.
 */
public class Param {
    public static final int FLOAT = 0;
    public static final int INTEGER = 1;
    public static final int STRING = 2;

    public static final Map<String, Integer> TYPES = Collections.unmodifiableMap(new HashMap<String, Integer>() {{
        put("Float", FLOAT);
        put("Integer", INTEGER);
        put("String", STRING);
        put("File", STRING);
    }});

    private int type;
    private Object value;

    public Param(int type, String value) {
        this.type = type;

        switch (type) {
            case FLOAT:
                this.value = Float.parseFloat(value);
                break;
            case INTEGER:
                this.value = Integer.parseInt(value);
                break;
            case STRING:
                this.value = value;
                break;
        }
    }

    public Param(int type, Float value) {
        this.type = type;

        if (type == FLOAT) {
            this.value = value;
        } else {
            throw new IncompatibleClassChangeError("Value must be of float type for using this constructor");
        }
    }

    public Object getValue() {
        return value;
    }

    public <T> T getValue(Class<T> klass) {
        return (T) value;
    }

    public int getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Params, type=" + type + ", value=" + value;
    }
}
