package org.caebeans.lib;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/4/12
 * Time: 2:06 PM
 * To change this template use File | Settings | File Templates.
 */
public interface LoggerManager {
    List<LogContent> getLogs();
//    void addTaskFileAppender(File instancePath) throws IOException;
    Logger getPrivateLogger(String dir, String instanceID) throws IOException;

    void info(Object message);
    void info(Object message, Throwable throwable);

    void debug(Object message);
    void debug(Object message, Throwable throwable);

    void warn(Object message);
    void warn(Object message, Throwable throwable);

    void error(Object message);
    void error(Object message, Throwable throwable);

    void fatal(Object message);
    void fatal(Object message, Throwable throwable);
}
