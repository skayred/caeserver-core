package org.caebeans.lib;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/5/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class RandomGenerator {
    private static final int ID_LENGTH = 25;

    public static String generateUniqueID() {
        SecureRandom random = new SecureRandom();

        return new BigInteger(ID_LENGTH * 5, random).toString(32);
    }
}
