package org.caebeans.lib;

import com.google.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/4/12
 * Time: 2:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoggerManagerImpl implements LoggerManager {
    static final Logger logger = Logger.getLogger(LoggerManagerImpl.class);
    public static final String CAEBEANS_LOGGER_PROPERTIES = ".caebeans/logger.properties";

    @Inject
    LoggerManagerImpl() throws FileNotFoundException {
        File userHome = FileUtils.getUserDirectory();

        File loggerProperties = new File(CAEBEANS_LOGGER_PROPERTIES);

        if (!loggerProperties.exists()) {
            loggerProperties = new File(userHome, CAEBEANS_LOGGER_PROPERTIES);
        } else {
        }

        if (loggerProperties.exists()) {
            PropertyConfigurator.configure(loggerProperties.getAbsolutePath());
        } else {
            throw new FileNotFoundException(loggerProperties.getAbsolutePath() + " not found");
        }
    }

    @Override
    public List<LogContent> getLogs() {
        List<LogContent> result = new ArrayList<LogContent>();

        Enumeration appenders = Logger.getRootLogger().getAllAppenders();

        while (appenders.hasMoreElements()) {
            Appender appender = (Appender) appenders.nextElement();

            if (appender instanceof FileAppender) {
                FileAppender fileAppender = (FileAppender) appender;

                try {
                    LogContent log = new LogContent();

                    log.setFilename(fileAppender.getName());
                    log.setFileContent(getFileContent(fileAppender.getFile()));

                    result.add(log);
                } catch (Exception e) {
                    fatal("Failed to get log content", e);
                }
            }
        }

        return result;
    }

    @Override
    public Logger getPrivateLogger(String dir, String instanceID) throws IOException {
        Logger logger = Logger.getLogger("exec" + instanceID);
        System.out.println(dir + "/execution_log.txt");

        Layout layout = new PatternLayout("%-5p [%d{dd MMM yyyy HH:mm:ss,SSS}]: %m\r\n");
        FileAppender appender = new FileAppender(layout, dir + "/execution_log.txt");

        logger.addAppender(appender);

        return logger;
    }

    @Override
    public void info(Object message) {
        logger.info(message);
    }

    @Override
    public void info(Object message, Throwable throwable) {
        logger.info(message, throwable);
    }

    @Override
    public void debug(Object message) {
        logger.debug(message);
    }

    @Override
    public void debug(Object message, Throwable throwable) {
        logger.debug(message, throwable);
    }

    @Override
    public void warn(Object message) {
        logger.warn(message);
    }

    @Override
    public void warn(Object message, Throwable throwable) {
        logger.warn(message, throwable);
    }

    @Override
    public void error(Object message) {
        logger.error(message);
    }

    @Override
    public void error(Object message, Throwable throwable) {
        logger.error(message, throwable);
    }

    @Override
    public void fatal(Object message) {
        logger.fatal(message);
    }

    @Override
    public void fatal(Object message, Throwable throwable) {
        logger.fatal(message, throwable);
    }

    private String getFileContent(String file) throws IOException {
        InputStream stream = new FileInputStream(file);

        System.out.println(stream);

        StringWriter writer = new StringWriter();
        IOUtils.copy(stream, writer);
        return writer.toString();
    }
}
