package org.caebeans.lib;

import com.google.inject.Inject;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/3/12
 * Time: 2:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class ConfigImpl implements Config {
    public static final String CAEBEANS_CAESERVER_PROPERTIES = ".caebeans/caeserver.properties";
    private PropertiesConfiguration config;

    @Inject
    ConfigImpl() {
        try {
            config = new PropertiesConfiguration(CAEBEANS_CAESERVER_PROPERTIES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Configuration getConfig() {
        return config;
    }
}
