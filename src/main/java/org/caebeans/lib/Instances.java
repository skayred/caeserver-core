package org.caebeans.lib;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/7/12
 * Time: 8:57 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Instances {
    int size();
    boolean contains(String id);
    void add(String id);
}
