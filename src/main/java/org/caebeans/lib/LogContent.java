package org.caebeans.lib;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/22/12
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class LogContent {
    private String filename;
    private String fileContent;

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
