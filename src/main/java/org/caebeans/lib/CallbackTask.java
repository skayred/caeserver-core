package org.caebeans.lib;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/26/12
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class CallbackTask implements Runnable {
    private final Runnable task;

    private final Callback callback;

    public CallbackTask(Runnable task, Callback callback) {
        this.task = task;
        this.callback = callback;
    }

    public void run() {
        task.run();
        callback.onComplete();
    }

    public static interface Callback {
        public void onComplete();
    }
}
