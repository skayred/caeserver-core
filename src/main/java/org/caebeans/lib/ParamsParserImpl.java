package org.caebeans.lib;

import org.caebeans.storage.WorkStorageManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 02.07.12
 * Time: 19:59
 * To change this template use File | Settings | File Templates.
 */
public class ParamsParserImpl implements ParamsParser {
    @Inject
    private WorkStorageManager workStorageManager;

    @Override
    public Map<String, Param> getParamsHash(String projectId, String instanceId) throws ParserConfigurationException, IOException, SAXException, IllegalStateException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        File problemCaebean = workStorageManager.getProblemCAEBean(projectId, instanceId);
        Document problem = dBuilder.parse(problemCaebean);
        problem.getDocumentElement().normalize();

        HashMap<String, Param> params = new HashMap<String, Param>();

        NodeList categories = problem.getElementsByTagName("category");

        for (int i = 0; i < categories.getLength(); i++) {
            Node category = categories.item(i);

            if (category.getNodeType() == Node.ELEMENT_NODE) {
                NodeList parameters = ((Element) category).getElementsByTagName("parameter");

                for (int j = 0; j < parameters.getLength(); j++) {
                    Node param = parameters.item(j);

                    if (param.getNodeType() == Node.ELEMENT_NODE) {
                        Element parameter = (Element) param;

                        params.put(parameter.getAttribute("name"), getParameter(parameter));
                    }
                }
            }
        }

        params.put("projectid", new Param(Param.STRING, projectId));

        return params;
    }

    private Param getParameter(Element paramNode) {
        String parameterAttribute = paramNode.getAttribute("type");

        if (!Param.TYPES.containsKey(parameterAttribute)) {
            throw new IllegalStateException("Wrong parameter type: " + parameterAttribute);
        }

        int type = Param.TYPES.get(parameterAttribute);
        String value;

        try {
            value = getTagValue("value", paramNode);
        } catch (NullPointerException ne) {
            value = getTagValue("default", paramNode);
        }

        Param param = new Param(type, value);

        return param;
    }

    private String getTagValue(String tag, Element element) {
        NodeList nlList = element.getElementsByTagName(tag).item(0).getChildNodes();

        Node nValue = nlList.item(0);

        return nValue.getNodeValue();
    }
}
