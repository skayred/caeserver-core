package org.caebeans.lib;

import org.apache.commons.configuration.Configuration;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/3/12
 * Time: 2:29 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Config {
    public Configuration getConfig();
}
