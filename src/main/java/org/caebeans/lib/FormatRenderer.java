package org.caebeans.lib;

import com.floreysoft.jmte.NamedRenderer;
import com.floreysoft.jmte.RenderFormatInfo;

import java.math.BigDecimal;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 11/8/12
 * Time: 8:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class FormatRenderer implements NamedRenderer {
    @Override
    public String render(Object o, String format, Locale locale) {
        return String.format(format, o);
    }

    @Override
    public String getName() {
        return "format";
    }

    @Override
    public RenderFormatInfo getFormatInfo() {
        return null;
    }

    @Override
    public Class<?>[] getSupportedClasses() {
        return new Class<?>[] {Integer.class, BigDecimal.class, Double.class, Float.class, Long.class};
    }
}
