package org.caebeans.lib;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/18/12
 * Time: 7:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class Serializer {
    public static Object deserialize(String serializedObject) throws IOException, ClassNotFoundException {
        byte[] data = Base64Coder.decode(serializedObject);
        ObjectInputStream ois = new ObjectInputStream(
                new ByteArrayInputStream(data));
        Object o = ois.readObject();
        ois.close();
        return o;
    }

    public static String serialize(Serializable object) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(object);
        oos.close();
        return new String(Base64Coder.encode(baos.toByteArray()));
    }
}
