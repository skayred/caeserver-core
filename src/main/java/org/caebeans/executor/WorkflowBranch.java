package org.caebeans.executor;

import org.apache.log4j.Logger;
import org.caebeans.lib.JmteEval;
import org.caebeans.lib.Param;
import org.caebeans.task.description.TaskDescription;
import org.caebeans.task.description.nodes.BaseNode;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/3/12
 * Time: 8:23 PM
 * To change this template use File | Settings | File Templates.
 */
public interface WorkflowBranch {
    Runnable createWorkflowBranch(ScheduledExecutorService scheduler,
                                  TaskDescription description,
                                  Map<String, Param> paramsHash,
                                  BaseNode node,
                                  JmteEval phpEval,
                                  Logger instanceLogger, Map<String, EndpointReferenceType> tss);
}
