package org.caebeans.executor;

import com.google.inject.Inject;
import org.apache.log4j.Logger;
import org.caebeans.database.WorkManager;
import org.caebeans.lib.JmteEval;
import org.caebeans.lib.LoggerManager;
import org.caebeans.lib.Param;
import org.caebeans.storage.WorkStorageManager;
import org.caebeans.task.description.TaskDescription;
import org.caebeans.task.description.nodes.BaseNode;
import org.caebeans.task.description.nodes.DecisionNode;
import org.caebeans.task.description.nodes.MergeNode;
import org.caebeans.task.description.nodes.NextNodePair;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/27/12
 * Time: 7:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class WorkflowBranchImpl implements WorkflowBranch {

    @Inject
    private WorkStorageManager workStorageManager;
    @Inject
    private LoggerManager logger;
    @Inject
    private WorkManager workManager;

    @Override
    public Runnable createWorkflowBranch(final ScheduledExecutorService scheduler,
                                         final TaskDescription description,
                                         final Map<String, Param> paramsHash,
                                         final BaseNode node,
                                         final JmteEval phpEval,
                                         final Logger instanceLogger,
                                         final Map<String, EndpointReferenceType> tss) {
        return new Runnable() {
            @Override
            public void run() {
                BaseNode currentNode = node;

                try {
                    while (currentNode.getType() != BaseNode.FINAL_NODE) {
//                        logger.info(currentNode);
                        boolean nextFound = false;

                        float executionResult = currentNode.exec(phpEval, instanceLogger, paramsHash, tss.get(currentNode.getId()));
                        paramsHash.put(currentNode.getId(), new Param(Param.FLOAT, executionResult));

                        System.out.println("Execution result: " + currentNode.getId() + " = " + executionResult);

                        switch (currentNode.getType()) {
                            case BaseNode.FORK_NODE:
                                for (int i = 1; i < currentNode.next.size(); i++) {
                                    BaseNode nextNode = currentNode.next.get(i).nextNode;
                                    scheduler.schedule(createWorkflowBranch(
                                            scheduler,
                                            description,
                                            paramsHash,
                                            nextNode,
                                            phpEval,
                                            instanceLogger,
                                            tss), 0, TimeUnit.SECONDS);
                                }
                                break;
                            case BaseNode.MERGE_NODE:
                                MergeNode currentMergeNode = (MergeNode) currentNode;
                                if (!currentMergeNode.incrementAndTest()) {
                                    return;
                                }
                                break;
                            case BaseNode.DECISION_NODE:
                                DecisionNode currentDecisionNode = (DecisionNode) currentNode;

                                for (NextNodePair nextPair : currentNode.next) {
                                    if (!currentDecisionNode.eval(nextPair.condition, paramsHash)) {
                                        continue;
                                    }

                                    currentNode = nextPair.nextNode;
                                    nextFound = true;

                                    System.out.println("Nest nodes: " + currentNode.getId());

                                    break;
                                }

                                break;
                        }

                        if (!nextFound) {
                            currentNode = currentNode.next.get(0).nextNode;
                        }
                    }

                    workStorageManager.deleteWorkingDirectory(description.getInstanceId(), description.outputs);
                    workManager.finishWork(description.getInstanceId());

                } catch (TimeoutException te) {
                    logger.fatal("Node execution timed out", te);
                    instanceLogger.fatal("Node execution timed out", te);

                    workManager.workTimedOut(description.getInstanceId());
                } catch (Exception e) {
                    logger.fatal("Node execution failed", e);
                    instanceLogger.fatal("Node execution failed", e);

                    workManager.failWork(description.getInstanceId());
                }
            }
        };
    }
}
