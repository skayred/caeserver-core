package org.caebeans.executor;

import org.apache.log4j.Logger;
import org.caebeans.lib.Param;
import org.caebeans.task.description.TaskDescription;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/26/12
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Executor {
    public void execute(TaskDescription description, Map<String, Param> paramsHash, Logger instanceLogger);
}
