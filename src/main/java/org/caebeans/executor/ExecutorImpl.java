package org.caebeans.executor;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Guice;
import org.apache.log4j.Logger;
import org.caebeans.broker.client.BrokerClient;
import org.caebeans.database.WorkManager;
import org.caebeans.lib.JmteEval;
import org.caebeans.lib.LoggerManager;
import org.caebeans.lib.Param;
import org.caebeans.lib.Config;
import org.caebeans.ServerModule;
import org.caebeans.storage.WorkStorageManager;
import org.caebeans.task.description.TaskDescription;
import org.caebeans.task.description.nodes.InitialNode;
import org.caebeans.xmlbeans.scheduler.cwf.ConcreteWorkflowDocument;
import org.caebeans.xmlbeans.scheduler.cwf.NodeDocument;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/26/12
 * Time: 2:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExecutorImpl implements Executor {
    private static final String POOL_SIZE = "caeserver.pool_size";

    private ScheduledExecutorService scheduler;
    private int nodesCount;
    private int nodesExecuted;
    private int threadPoolSize;

    @Inject private WorkStorageManager workStorageManager;
    @Inject private WorkflowBranch workflowBranch;
    @Inject private LoggerManager logger;
    @Inject private WorkManager workManager;
    @Inject private BrokerClient brokerClient;

    @Inject
     ExecutorImpl() {
        Injector injector = Guice.createInjector(new ServerModule());
        Config config = injector.getInstance(Config.class);
        threadPoolSize = config.getConfig().getInt(POOL_SIZE);

        scheduler = Executors.newScheduledThreadPool(threadPoolSize);
    }

    @Override
    public void execute(TaskDescription description, Map<String, Param> paramsHash, Logger instanceLogger) {
        final InitialNode node = description.getInitialNode();

        logger.info("Execution: instance " + description.getInstanceId() + ", project " + description.getProjectId());
        logger.info("THREAD POOL SIZE " + threadPoolSize);
        instanceLogger.info("Execution: instance " + description.getInstanceId() + ", project " + description.getProjectId());

        Map<String, EndpointReferenceType> tss = new HashMap<String, EndpointReferenceType>();
        try {
            tss = getTSS(brokerClient.seachTSS(description.asAbstractWorkflow()));
        } catch (Exception e) {
            logger.fatal("Failed to get concrete workflow from the broker", e);
            return;
        }

        workManager.startWork(description.getInstanceId());
        Map<String, Param> params = new HashMap<String, Param>();
        try {
            params = workStorageManager.getProblemParameters(description.getProjectId(), description.getInstanceId());
        } catch (Exception e) {
            logger.error("Failed to read problem CAEBean!", e);
            instanceLogger.error("Failed to read problem CAEBean!", e);
            e.printStackTrace();
        }

        JmteEval jmteEval = new JmteEval(params);

        Runnable worker = workflowBranch.createWorkflowBranch(scheduler, description, paramsHash, node, jmteEval, instanceLogger, tss);
        scheduler.schedule(worker, 0, TimeUnit.SECONDS);
    }

    private Map<String, EndpointReferenceType> getTSS(ConcreteWorkflowDocument.ConcreteWorkflow workflow) {
        Map<String,EndpointReferenceType> result = new HashMap<String, EndpointReferenceType>();

        for (NodeDocument.Node node : workflow.getNodeArray()) {
            if (node != null) {
                logger.error("CWF node " + node.getId());

                if (node.getType().getACTIVITY() != null) {
                    result.put(node.getId(), node.getType().getACTIVITY().getTargetSystemService());
                }
            }
        }

        return result;
    }
}
