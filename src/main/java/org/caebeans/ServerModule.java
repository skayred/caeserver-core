package org.caebeans;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import org.caebeans.broker.client.BrokerClient;
import org.caebeans.broker.client.BrokerClientImpl;
import org.caebeans.database.ProjectDescriptionManager;
import org.caebeans.database.ProjectDescriptionManagerImpl;
import org.caebeans.database.WorkManager;
import org.caebeans.database.WorkManagerImpl;
import org.caebeans.executor.Executor;
import org.caebeans.executor.ExecutorImpl;
import org.caebeans.executor.WorkflowBranch;
import org.caebeans.executor.WorkflowBranchImpl;
import org.caebeans.lib.*;
import org.caebeans.storage.*;
import org.caebeans.task.builder.TaskBuilder;
import org.caebeans.task.builder.TaskBuilderImpl;
import org.caebeans.unicore.client.Client;
import org.caebeans.unicore.client.ClientImpl;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/24/12
 * Time: 9:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class ServerModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Config.class).to(ConfigImpl.class).in(Singleton.class);
        bind(Client.class).to(ClientImpl.class).in(Singleton.class);
        bind(LoggerManager.class).to(LoggerManagerImpl.class).in(Singleton.class);
        bind(WorkflowBranch.class).to(WorkflowBranchImpl.class).in(Singleton.class);
        bind(ProjectStorageManager.class).to(ProjectStorageManagerImpl.class).in(Singleton.class);
        bind(WorkStorageManager.class).to(WorkStorageManagerImpl.class).in(Singleton.class);
        bind(ResultStorageManager.class).to(ResultStorageManagerImpl.class).in(Singleton.class);
        bind(Instances.class).to(InstancesImpl.class).in(Singleton.class);
        bind(WorkManager.class).to(WorkManagerImpl.class).in(Singleton.class);
        bind(ProjectDescriptionManager.class).to(ProjectDescriptionManagerImpl.class).in(Singleton.class);
        bind(ParamsParser.class).to(ParamsParserImpl.class).in(Singleton.class);
        bind(BrokerClient.class).to(BrokerClientImpl.class).in(Singleton.class);

        bind(TaskBuilder.class).to(TaskBuilderImpl.class);
        bind(Executor.class).to(ExecutorImpl.class);
    }
}
