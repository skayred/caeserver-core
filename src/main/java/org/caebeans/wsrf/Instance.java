package org.caebeans.wsrf;

import org.caebeans.caeserver.GetStatus;
import org.caebeans.caeserver.GetStatusResponse;
import org.caebeans.caeserver.SubmitJob;
import org.caebeans.caeserver.SubmitJobResponse;

import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlMimeType;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/6/12
 * Time: 8:46 PM
 * To change this template use File | Settings | File Templates.
 */
@WebService(targetNamespace = "http://java.CAEServer", portName = "CAEInstance")
public interface Instance  {
    @WebMethod(action = "http://java.CAEServer/submitJob")
    SubmitJobResponse submitJob(SubmitJob submitJobDocument, int cpuCount, int memoryVolume);

    @WebMethod(action = "http://java.CAEServer/getStatus")
    GetStatusResponse getStatus(GetStatus instance);

    @WebMethod(action = "http://java.CAEServer/getExecTime")
    long getExecTime(org.caebeans.caeserver.Instance instance);

//    @WebMethod(action = "http://java.CAEServer/getResultsArch")
//    String getResultsArch(org.caebeans.caeserver.Instance instance);
//
//    @WebMethod(action = "http://java.CAEServer/getWorkArch")
//    String getWorkArch(org.caebeans.caeserver.Instance instance);

    @WebMethod(action = "http://java.CAEServer/removeResultDirectory")
    void removeResultDir(org.caebeans.caeserver.Instance instance);

    @WebMethod(action = "http://java.CAEServer/uploadUpdates")
    void uploadUpdates(String projectID, @XmlMimeType("application/octet-stream") DataHandler content);
}
