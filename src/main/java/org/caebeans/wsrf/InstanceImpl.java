package org.caebeans.wsrf;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.sun.xml.ws.developer.StreamingAttachment;
import org.apache.log4j.Logger;
import org.caebeans.ServerModule;
import org.caebeans.caeserver.*;
import org.caebeans.database.ProjectDescriptionManager;
import org.caebeans.database.WorkManager;
import org.caebeans.executor.Executor;
import org.caebeans.lib.Instances;
import org.caebeans.lib.LoggerManager;
import org.caebeans.lib.ParamsParser;
import org.caebeans.models.Work;
import org.caebeans.storage.ProjectStorageManager;
import org.caebeans.storage.ResultStorageManager;
import org.caebeans.storage.WorkStorageManager;
import org.caebeans.task.description.TaskDescription;
import org.xml.sax.SAXException;

import javax.activation.DataHandler;
import javax.jws.Oneway;
import javax.jws.WebService;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.BindingType;
import javax.xml.ws.soap.MTOM;
import javax.xml.ws.soap.SOAPBinding;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/7/12
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */
@WebService(endpointInterface = "org.caebeans.wsrf.Instance")
public class InstanceImpl implements Instance {

    @Inject
    private ProjectStorageManager projectStorageManager;
    @Inject
    private ProjectDescriptionManager descriptionManager;
    @Inject
    private WorkStorageManager workStorageManager;
    @Inject
    private ResultStorageManager resultStorageManager;
    @Inject
    private Executor executor;
    @Inject
    private LoggerManager logger;
    @Inject
    private WorkManager workManager;
    @Inject
    private ParamsParser paramsParser;

    private Instances instances;
    private ObjectFactory objectFactory;

    public InstanceImpl(Instances instances) {
        Injector injector = Guice.createInjector(new ServerModule());
        injector.injectMembers(this);

        this.instances = instances;
        this.objectFactory = new ObjectFactory();
    }

    @Override
    public SubmitJobResponse submitJob(SubmitJob submitJobDocument, int cpuCount, int memoryVolume) {
        String instanceID = submitJobDocument.getInstance().getId();

        if (!instances.contains(instanceID)) {
            logger.fatal("No such instance");
            throw new RuntimeException("No such instance");
        }

        String projectId = cleanupId(submitJobDocument.getProblemCaebean().getCaebeanId());

        workManager.createWork(instanceID, projectId, memoryVolume, cpuCount);
//        workStorageManager.createWorkingDirectory(projectId, instanceID);

        Logger instanceLogger = null;
        try {
            instanceLogger = logger.getPrivateLogger(workStorageManager.getWorkingDirectory(instanceID).getAbsolutePath(), instanceID);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        try {
            workStorageManager.saveProblemCAEBean(projectId, instanceID, submitJobDocument.getProblemCaebean(), instanceLogger);

            runWork(projectId, instanceID, instanceLogger);

            Result result = new Result();
            result.setValue(0);

            SubmitJobResponse responseDocument = objectFactory.createSubmitJobResponse();
            responseDocument.setResult(result);

            return responseDocument;
        } catch (Exception ie) {
            ie.printStackTrace();

            workManager.failWork(instanceID);

            logger.fatal("Failed to save problem CAEBean", ie);
            instanceLogger.fatal("Failed to save problem CAEBean", ie);
            throw new RuntimeException("Failed to save problem CAEBean: " + ie.getMessage());
        }
    }


    @Override
    public GetStatusResponse getStatus(GetStatus instance) {
        int status;

        try {
            status = workManager.getStatus(instance.getInstance().getId());
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            throw new RuntimeException("No such instance");
        }

        GetStatusResponse getStatusResponse = objectFactory.createGetStatusResponse();

        Status statusObject = new Status();
        switch (status) {
            case Work.STATUS_UNSTARTED:
                statusObject.setNOTSTARTED("");
                break;
            case Work.STATUS_STARTED:
                statusObject.setRUNNING("");
                break;
            case Work.STATUS_FINISHED:
                statusObject.setSUCCESSFULL("");
                break;
            case Work.STATUS_FAILED:
                statusObject.setFAILED("");
                break;
        }

        getStatusResponse.setStatus(statusObject);

        return getStatusResponse;
    }

    @Override
    public long getExecTime(org.caebeans.caeserver.Instance instance) {
        return workManager.getExecutionTime(instance.getId());
    }

//    @Override
//    public String getResultsArch(org.caebeans.caeserver.Instance instance) {
//        try {
//            return resultStorageManager.getResultPackage(instance.getId());
//        } catch (Exception e) {
//
//            System.out.println("--------------");
//            e.printStackTrace();
//            System.out.println("--------------");
//
//            logger.fatal("Failed to zip results", e);
//            throw new RuntimeException("Failed to zip results");
//        }
//    }
//
//    @Override
//    public String getWorkArch(org.caebeans.caeserver.Instance instance) {
//        try {
//            return workStorageManager.getWorkPackage(instance.getId());
//        } catch (Exception e) {
//            logger.fatal("Failed to zip work", e);
//            throw new RuntimeException("Failed to zip results");
//        }
//    }

    @Override
    public void removeResultDir(org.caebeans.caeserver.Instance instance) {
        try {
        resultStorageManager.removeResults(instance.getId());
        } catch (Exception e) {
            logger.fatal("Failed to remove results");
            throw new RuntimeException("Failed to remove results");
        }
    }

    @Override
    public void uploadUpdates(String projectID, DataHandler content) {
        workStorageManager.updateProjectFiles(projectID, content);
    }

    public void checkForUnfinished() {
        List<Work> works = workManager.getUnfinishedWorks();

        for (Work work : works) {
            try {
                Logger instanceLogger = logger.getPrivateLogger(
                        workStorageManager.getWorkingDirectory(work.getInstanceId()).getAbsolutePath(), work.getInstanceId());
                runWork(work.getProjectID(), work.getInstanceId(), instanceLogger);
            } catch (Exception e) {
                logger.fatal("Failed to rerun unfinished jobs", e);
            }
        }
    }

    private void runWork(String projectId, String instanceID, Logger instanceLogger) throws
            IOException, ClassNotFoundException, ParserConfigurationException, SAXException, IllegalStateException {
        TaskDescription description = descriptionManager.getDescription(projectId);
        description.setInstanceId(instanceID);
        description.addOutput("execution_log.txt");

        instanceLogger.info("Description: " + description);

        executor.execute(description, paramsParser.getParamsHash(projectId, instanceID), instanceLogger);
    }

    private String cleanupId(String caebeanId) {
        return caebeanId.replace("{", "").replace("}", "");
    }
}
