package org.caebeans.wsrf;

import org.caebeans.caeserver.CreateInstanceResponse;
import org.caebeans.caeserver.SubmitJob;

import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/6/12
 * Time: 8:44 PM
 * To change this template use File | Settings | File Templates.
 */
@WebService(targetNamespace = "http://java.CAEServer", portName = "CAEServer")
public interface CAEServer  {

    @WebMethod(action = "http://java.CAEServer/createInstance")
    CreateInstanceResponse createInstance(String projectID);

//    @WebMethod(action = "http://java.CAEServer/uploadProject")
//    void uploadProject(DataHandler content);

    @WebMethod(action = "http://java.CAEServer/indexProject")
    void indexProject(String projectId);

    @WebMethod(action = "http://java.CAEServer/indexAllProjects")
    void indexAllProjects();

    @WebMethod(action = "http://java.CAEServer/getProjectIDs")
    List<String> getProjectsIDs();

    @WebMethod(action = "http://java.CAEServer/generateID")
    public String generateID();

    @WebMethod(action = "http://java.CAEServer/getProblemCAEBeanByID")
    public SubmitJob getProblemCAEBeanByID(String projectID, String problemID);

    @WebMethod(action = "http://java.CAEServer/getProblemCaebeans")
    public Set<String> getProblemCAEBeans(String projectID);
}
