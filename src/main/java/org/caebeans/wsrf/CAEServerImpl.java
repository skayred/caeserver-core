package org.caebeans.wsrf;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.caebeans.ServerModule;
import org.caebeans.caeserver.*;
import org.caebeans.caeserver.Instance;
import org.caebeans.database.ProjectDescriptionManager;
import org.caebeans.lib.Instances;
import org.caebeans.lib.LoggerManager;
import org.caebeans.lib.RandomGenerator;
import org.caebeans.storage.ProjectStorageManager;
import org.caebeans.storage.WorkStorageManager;

import javax.activation.DataHandler;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/7/12
 * Time: 2:15 PM
 * To change this template use File | Settings | File Templates.
 */
@WebService(endpointInterface = "org.caebeans.wsrf.CAEServer")
public class CAEServerImpl implements CAEServer {
    public static final String INSTANCE_ID_PARAMETER_NAME = "instance_id";

    @Inject
    private ProjectStorageManager projectStorage;
    @Inject
    private ProjectDescriptionManager projectDescriptionManager;
    @Inject
    private WorkStorageManager workStorageManager;
    @Inject
    private LoggerManager logger;

    private Instances instances;
    private ObjectFactory objectFactory;

    public CAEServerImpl(Instances instances) {
        Injector injector = Guice.createInjector(new ServerModule());
        injector.injectMembers(this);

        this.instances = instances;
        this.objectFactory = new ObjectFactory();
    }

    @Override
    public CreateInstanceResponse createInstance(String projectID) {
        String ID = RandomGenerator.generateUniqueID();

        while (instances.contains(ID)) {
            ID = RandomGenerator.generateUniqueID();
        }

        instances.add(ID);

        workStorageManager.createWorkingDirectory(projectID, ID);

        Instance instance = objectFactory.createInstance();
        instance.setId(ID);

        CreateInstanceResponse response = objectFactory.createCreateInstanceResponse();
        response.setInstance(instance);

        return response;
    }

//    @Override
//    public void uploadProject(DataHandler content) {
//        projectStorage.loadProject(content);
//    }

    @Override
    public void indexProject(String projectId) {
        projectStorage.indexProject(projectId);
    }

    @Override
    public void indexAllProjects() {
        projectStorage.indexAllProjects();
    }

    @Override
    public List<String> getProjectsIDs() {
        try {
            return projectDescriptionManager.getProjectsIDs();
        } catch (Exception ce) {
            logger.fatal("Failed to get description list", ce);

            return new ArrayList<String>();
        }
    }

    @Override
    public String generateID() {
        try {
            return projectDescriptionManager.generateProjectID();
        } catch (Exception e) {
            logger.fatal("Failed to generate unique ID", e);

            return "";
        }
    }

    @Override
    public Set<String> getProblemCAEBeans(String projectID) {
        try {
            return projectDescriptionManager.getDescription(projectID).getProblemCAEBeans();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return null;
    }

    @Override
    public SubmitJob getProblemCAEBeanByID(String projectID, String problemID) {
        try {
            ProblemCaebean problemCaebean = projectStorage.getProblemCaebean(projectID, problemID);

            SubmitJob response = new SubmitJob();
            response.setProblemCaebean(problemCaebean);
            return response;
        } catch (JAXBException e) {
            e.printStackTrace();
            logger.fatal("Failed to parse problem caebean", e);

            throw new RuntimeException("Failed to parse problem caebean");
        }
    }
}
