package org.caebeans.unicore.client;

import com.google.inject.Inject;
import com.hazelcast.core.MapEntry;
import de.fzj.unicore.uas.TargetSystemFactory;
import de.fzj.unicore.uas.client.*;
import de.fzj.unicore.uas.security.ClientProperties;
import de.fzj.unicore.ucc.utils.FileDownloader;
import de.fzj.unicore.ucc.utils.FileUploader;
import de.fzj.unicore.ucc.utils.Mode;
import de.fzj.unicore.wsrflite.xmlbeans.WSUtilities;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.log4j.Logger;
import org.caebeans.lib.Config;
import org.caebeans.lib.LoggerManager;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ApplicationDocument;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ApplicationType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.EnvironmentType;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.LimitsType;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.POSIXApplicationDocument;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.POSIXApplicationType;
import org.unigrids.x2006.x04.services.tss.SubmitDocument;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/1/12
 * Time: 10:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClientImpl implements Client {
    private static final String SSL_KEYSTORE = "wsrf.ssl.keystore";
    private static final String SSL_KEYPASS = "wsrf.ssl.keypass";
    private static final String SSL_KEYALIAS = "wsrf.ssl.keyalias";
    private static final String SSL_TRUSTSTORE = "wsrf.ssl.truststore";
    private static final String SSL_TRUSTPASS = "wsrf.ssl.trustpass";
    public static final String UNICORE_REGISTRY_URL = "unicore.registry_url";
    private static final int MAX_EXECUTION_TIME = 43200;

    private RegistryClient registry;
    ExecutorService service = Executors.newFixedThreadPool(1);
    ScheduledExecutorService canceller = Executors.newSingleThreadScheduledExecutor();

    public <T> Future<T> executeTask(Callable<T> c, long timeoutMS) {
        final Future<T> future = service.submit(c);

        canceller.schedule(new Callable<Void>(){
            public Void call(){
                future.cancel(true);
                return null;
            }
        }, timeoutMS, TimeUnit.SECONDS);

        return future;
    }

    @Inject    private Config config;

    @Inject    private LoggerManager logger;

    @Inject
    ClientImpl() {

    }

    @Override
    public void run(String dir, String appName, String appVersion, Map<String, String> parameters, List<String> inputs, List<String> outputs, Logger instanceLogger, EndpointReferenceType tss) throws Exception {
        registry = initRegistryClient();

        for (Map.Entry<String, String> param : parameters.entrySet()) {
            System.out.println("Param: " + param.getKey() + " -> " + param.getValue());
        }

        final JobClient jobClient = createJob(appName, appVersion, parameters, tss);
        StorageClient storageClient = jobClient.getUspaceClient();

        for (String input : inputs) {
            File directory = new File(dir);
            FileFilter fileFilter = new WildcardFileFilter(input);
            File[] realInputs = directory.listFiles(fileFilter);

            for (File realInput : realInputs) {
                FileUploader uploader = new FileUploader(realInput.getAbsolutePath(), "/" + realInput.getName(), Mode.overwrite);

                try {
                    uploader.perform(storageClient, new CAEMessageWriter());
                } catch (FileNotFoundException fnfe) {
                    logger.error("Input file not found", fnfe);
                    instanceLogger.error("Input file not found", fnfe);
                }
            }
        }

        long currentTime = System.currentTimeMillis();

        jobClient.waitUntilReady(0);
        jobClient.start();
        executeTask(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                System.out.println("Wait until done");

                try {
                    jobClient.waitUntilDone(0);
                } catch (InterruptedException ie) {
                    System.out.println("Job aborted");

                    jobClient.abort();
                }

                System.out.println("Job finished or aborted");

                return null;
            }
        }, MAX_EXECUTION_TIME);
        
        try {
            jobClient.waitUntilDone(0);
        } catch (Exception e) {
            throw new TimeoutException("Work timed out");
        }

        logger.warn("ETIME: Execution " + (System.currentTimeMillis() - currentTime) / 1000);
        currentTime = System.currentTimeMillis();

        logger.warn("Current appname is " + appName);
        instanceLogger.warn("Current appname is " + appName);

        for (String output : outputs) {
            FileDownloader downloader = new FileDownloader(output, dir, Mode.overwrite);

            try {
                downloader.perform(storageClient, new CAEMessageWriter());
            } catch (FileNotFoundException fnfe) {
                logger.error("Output file not found", fnfe);
                instanceLogger.error("Output file not found", fnfe);
            }
        }
        logger.warn("ETIME: Downloading " + (System.currentTimeMillis() - currentTime) / 1000);
    }

    private ClientProperties setupSecurity() {
        ClientProperties sp = new ClientProperties(new Properties());
        sp.setProperty(ClientProperties.WSRF_SSL, "true");

        Configuration conf = config.getConfig();

        sp.setProperty(ClientProperties.WSRF_SSL_KEYSTORE, conf.getString(SSL_KEYSTORE));
        sp.setProperty(ClientProperties.WSRF_SSL_KEYPASS, conf.getString(SSL_KEYPASS));
        sp.setProperty(ClientProperties.WSRF_SSL_KEYALIAS, conf.getString(SSL_KEYALIAS));
        sp.setProperty(ClientProperties.WSRF_SSL_TRUSTSTORE, conf.getString(SSL_TRUSTSTORE));
        sp.setProperty(ClientProperties.WSRF_SSL_TRUSTPASS, conf.getString(SSL_TRUSTPASS));

        return sp;
    }

    private RegistryClient initRegistryClient() throws Exception {
        String url = config.getConfig().getString(UNICORE_REGISTRY_URL);
        EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
        epr.addNewAddress().setStringValue(url);
        RegistryClient registryClient = new RegistryClient(url, epr, setupSecurity());

        return registryClient;
    }

    private TSSClient createTSSClient(RegistryClient registry, EndpointReferenceType tss) throws Exception {
        logger.warn("Using TSS " + tss);

        return new TSSClient(tss, registry.getSecurityProperties());

/*
        List<EndpointReferenceType> tsfEPRs = registry.listAccessibleServices(TargetSystemFactory.TSF_PORT);
        EndpointReferenceType epr = tsfEPRs.get(0);
//        String url = epr.getAddress().getStringValue();
        String url = "https://10.0.40.156:8080/SOZYKIN-SITE/services/TargetSystemFactoryService?res=default_target_system_factory";
        logger.warn("URL: " + url);

        TSFClient tsf = new TSFClient(url, epr, registry.getSecurityProperties());

        List<EndpointReferenceType> availableTSS = tsf.getAccessibleTargetSystems();

        if (availableTSS.size() > 0) {
            return new TSSClient(availableTSS.get(0), registry.getSecurityProperties());
        } else {
            return tsf.createTSS();
        }
*/
    }

    private JobClient createJob(String appName, String appVersion, Map<String, String> parameters, EndpointReferenceType tss) throws Exception {
        TSSClient targetSystem = createTSSClient(registry, tss);

        ApplicationDocument ad = ApplicationDocument.Factory.newInstance();
        ApplicationType app = ad.addNewApplication();

        SubmitDocument submitDoc = SubmitDocument.Factory.newInstance();
        JobDefinitionType job = submitDoc.addNewSubmit().addNewJobDefinition();
        app.setApplicationName(appName);
        app.setApplicationVersion(appVersion);

        POSIXApplicationDocument pd = POSIXApplicationDocument.Factory.newInstance();
        POSIXApplicationType p = pd.addNewPOSIXApplication();

        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            EnvironmentType et = p.addNewEnvironment();
            et.setName(entry.getKey());
            et.setStringValue(entry.getValue());
        }

        WSUtilities.append(pd, ad);

        job.addNewJobDescription().setApplication(app);

        JobClient jobClient = targetSystem.submit(submitDoc);

        return jobClient;
    }
}
