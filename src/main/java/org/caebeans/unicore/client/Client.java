package org.caebeans.unicore.client;

import org.apache.log4j.Logger;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/3/12
 * Time: 7:58 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Client {
    void run(String dir, String appName, String appVersion, Map<String, String> parameters, List<String> inputs, List<String> outputs, Logger instanceLogger, EndpointReferenceType tss) throws Exception;
}
