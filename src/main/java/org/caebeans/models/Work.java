package org.caebeans.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/9/12
 * Time: 8:07 PM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "Work")
public class Work {
    public static final int STATUS_UNSTARTED = 0;
    public static final int STATUS_STARTED = 1;
    public static final int STATUS_FINISHED = 2;
    public static final int STATUS_FAILED = 3;
    public static final int STATUS_TIME_LIMIT = 4;

    @Id
    @Column(name = "instance_id")      private String instanceId;
    @Column(name = "status")           private int status;
    @Column(name = "start_time")       private long startTime;
    @Column(name = "end_time")         private long endTime;
    @Column(name = "project_id")       private String projectID;
    @Column(name = "memory_volume")    private int memory;
    @Column(name = "cpu_count")        private int cpuCount;

    public Work() {
        status = STATUS_UNSTARTED;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public int getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(int cpuCount) {
        this.cpuCount = cpuCount;
    }
}
