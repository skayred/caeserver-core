package org.caebeans.models;

import org.caebeans.lib.Serializer;
import org.caebeans.task.description.TaskDescription;

import javax.persistence.*;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/18/12
 * Time: 7:15 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "ProjectDescription")
public class Description {
    @Id
    @Column(name="project_id")
    private String projectId;

    @Column(name = "project_description", columnDefinition="CLOB")
    private String descriptionSerialized;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public TaskDescription getDescription() throws ClassNotFoundException, IOException {
        return (TaskDescription) Serializer.deserialize(descriptionSerialized);
    }

    public void setDescription(TaskDescription description) throws IOException {
        this.descriptionSerialized = Serializer.serialize(description);
    }
}
