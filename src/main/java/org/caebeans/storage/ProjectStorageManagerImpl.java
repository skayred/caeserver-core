package org.caebeans.storage;

import com.google.inject.Inject;
import org.caebeans.caeserver.ProblemCaebean;
import org.caebeans.database.ProjectDescriptionManager;
import org.caebeans.lib.Config;
import org.caebeans.lib.LoggerManager;
import org.caebeans.task.builder.TaskBuilder;
import org.caebeans.task.description.TaskDescription;
import org.w3c.dom.Document;

import javax.activation.DataHandler;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/22/12
 * Time: 9:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProjectStorageManagerImpl extends StorageManager implements ProjectStorageManager {
    public static final String STORAGE_LOCATION_KEY = "storage.projects";

    @Inject private TaskBuilder taskBuilder;
    @Inject private ProjectDescriptionManager projectDescriptionManager;
    @Inject private LoggerManager logger;

    @Inject
    ProjectStorageManagerImpl(Config config) {
        super(config);
    }

    @Override
    public void loadProject(String filePath) {
        try {
            loadProject(new FileInputStream(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        new File(filePath).delete();
    }

    @Override
    public void loadProject(DataHandler projectContent) {
        InputStream inputStream = null;
        try {
            inputStream = projectContent.getInputStream();

            loadProject(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadProject(InputStream inputStream) {
        logger.info("Loading project");

        File projectsStorage = new File(getBuilder().toString());
        String projectId = null;

        try {
            BufferedOutputStream destination;
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(inputStream));

            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                String[] nameSegments = entry.getName().split("/");

                if ((entry.getName().contains("__MACOSX")) || (nameSegments[nameSegments.length - 1].startsWith("."))) {
                    continue;
                }

                if (entry.isDirectory()) {
                    logger.info("Extracting directory: " + entry.getName());
                    (new File(projectsStorage, entry.getName())).mkdir();
                    continue;
                }

                logger.info("Extracting: " + entry);

                int count;
                byte data[] = new byte[BUFFER];
                FileOutputStream fos = new FileOutputStream(new File(projectsStorage, entry.getName()));
                destination = new BufferedOutputStream(fos, BUFFER);

                while ((count = zis.read(data, 0, BUFFER)) != -1) {
                    destination.write(data, 0, count);
                }

                destination.flush();
                destination.close();

                if (entry.getName().endsWith("Project.xml")) {
                    File projectFile = new File(projectsStorage, entry.getName());

                    projectId = getProjectID(projectFile);
                }
            }

            zis.close();
        } catch (IOException e) {
            logger.fatal("Failed to deploy project", e);
        }

        logger.info("Project deployed. Indexing...");

        if (projectId != null) {
            indexProject(projectId);
        } else {
            logger.fatal("Project ID not found");
        }
    }

    private String getProjectID(File projectFile) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document project = dBuilder.parse(projectFile);
            project.getDocumentElement().normalize();

            return project.getDocumentElement().getAttribute("id").replace("{", "").replace("}", "");
        } catch (Exception e) {
            logger.fatal("Failed to get project ID", e);

            return null;
        }
    }

    @Override
    public void indexProject(String projectId) {
        try {
            TaskDescription description = taskBuilder.build(projectId);

            projectDescriptionManager.loadDescription(description);
        } catch (Exception e) {
            logger.fatal("Failed to index project", e);
        }
    }

    @Override
    public void indexAllProjects() {
        File files[];
        File path = new File(getBuilder().toString());

        files = path.listFiles();

        dropIndex();

        for (File file : files) {
            if (file.isDirectory()) {
                System.out.println("Indexing " + file.getName());

                indexProject(file.getName());
            }
        }
    }

    @Override
    public ProblemCaebean getProblemCaebean(String projectID, String problemID) throws JAXBException {
        final JAXBContext context = JAXBContext.newInstance(ProblemCaebean.class);
        final Unmarshaller unmarshaller = context.createUnmarshaller();

        return  (ProblemCaebean) unmarshaller.unmarshal(getProjectProblemCaebean(projectID, problemID));
    }

    @Override
    public File getProjectDescription(String projectId) {
        logger.info("Getting project description for project " + projectId);

        StringBuilder projectDescriptionBuilder = getBuilder();

        projectDescriptionBuilder
                .append(projectId)
                .append("/Project.xml");

        return new File(projectDescriptionBuilder.toString());
    }

    private File getProjectProblemCaebean(String projectId, String problemID) {
        logger.info("Getting project description for project " + projectId);

        StringBuilder projectDescriptionBuilder = getBuilder();

        projectDescriptionBuilder
                .append(projectId)
                .append("/")
                .append(problemID)
                .append(".xml");

        return new File(projectDescriptionBuilder.toString());
    }

    @Override
    public File getProjectDirectory(String projectId) {
        logger.info("Getting project directory for project " + projectId);

        StringBuilder projectPathBuilder = getBuilder();

        projectPathBuilder.append(projectId);

        return new File(projectPathBuilder.toString());
    }

    @Override
    protected String getConfigKey() {
        return STORAGE_LOCATION_KEY;
    }

    private void dropIndex() {
        projectDescriptionManager.deleteAllDescriptions();
    }
}
