package org.caebeans.storage;

import org.caebeans.caeserver.ProblemCaebean;

import javax.activation.DataHandler;
import javax.xml.bind.JAXBException;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/24/12
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ProjectStorageManager {
    void loadProject(DataHandler projectContent);

    void loadProject(String path);

    File getProjectDescription(String projectId);

    File getProjectDirectory(String projectId);

    void indexProject(String projectId);

    void indexAllProjects();

    ProblemCaebean getProblemCaebean(String projectID, String problemID) throws JAXBException;
}
