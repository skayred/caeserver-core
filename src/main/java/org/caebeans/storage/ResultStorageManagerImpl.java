package org.caebeans.storage;

import com.google.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.caebeans.lib.Config;
import org.caebeans.lib.LoggerManager;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/3/12
 * Time: 8:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResultStorageManagerImpl extends StorageManager implements ResultStorageManager {
    public static final String STORAGE_LOCATION_KEY = "storage.results";

    @Inject
    LoggerManager logger;

    @Inject
    ResultStorageManagerImpl(Config config) {
        super(config);
    }

    @Override
    public File getResultDirectory(String instanceId) {
        logger.info("Creating result directory for instance " + instanceId);

        StringBuilder projectPathBuilder = getBuilder();

        projectPathBuilder.append(instanceId);

        File resultDir = new File(projectPathBuilder.toString());
        resultDir.mkdir();

        return resultDir;
    }

    @Override
    public String getResultPackage(String instanceId) throws IOException {
        return zipFolder(getResultDirectory(instanceId), instanceId);
    }

    @Override
    public void removeResults(String id) throws IOException {
        FileUtils.deleteDirectory(getResultDirectory(id));
    }

    @Override
    protected String getConfigKey() {
        return STORAGE_LOCATION_KEY;
    }
}
