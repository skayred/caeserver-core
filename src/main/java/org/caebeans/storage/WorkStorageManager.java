package org.caebeans.storage;

import org.apache.log4j.Logger;
import org.caebeans.caeserver.ProblemCaebean;
import org.caebeans.lib.Param;
import org.xml.sax.SAXException;

import javax.activation.DataHandler;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/3/12
 * Time: 12:17 PM
 * To change this template use File | Settings | File Templates.
 */
public interface WorkStorageManager {
    public void saveProblemCAEBean(String projectId, String instanceId, ProblemCaebean document, Logger instanceLogger) throws IOException;

    public File getProblemCAEBean(String projectId, String instanceId);

    public void createWorkingDirectory(String projectId, String instanceId);

    public void deleteWorkingDirectory(String instanceId, Set<String> outputs);

    public File createNodeDirectory(String instanceId, String nodeName, List<String> inputs, Logger instanceLogger);

    public void deleteNodeDirectory(String instanceId, String nodeName, List<String> outputs, Logger instanceLogger);

    public File getWorkingDirectory(String instanceId);

    public String getPreprocess(String instanceId, String execName);

    public Map<String, Param> getProblemParameters(String projectId, String instanceId) throws ParserConfigurationException, IOException, SAXException;

    String getWorkPackage(String id) throws IOException;

    void updateProjectFiles(String projectID, DataHandler projectContent);
}
