package org.caebeans.storage;

import com.google.inject.Inject;
import org.caebeans.lib.Config;
import org.caebeans.lib.LoggerManager;
import org.herasaf.xacml.core.types.DateTime;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/3/12
 * Time: 3:08 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class StorageManager {
    protected static final int BUFFER = 2048;
    private static final String TMP_ZIP_FILE = "tmp.zip";
    private static final String STORAGE_ZIP_KEY = "storage.results_zip_dir";

    @Inject
    private Config config;
    @Inject
    private LoggerManager logger;

    private String projectsPath;
    private String zipStoragePath;

    @Inject
    StorageManager(Config config) {
        zipStoragePath = config.getConfig().getString(STORAGE_ZIP_KEY);
    }

    protected abstract String getConfigKey();

    protected StringBuilder getBuilder() {
        StringBuilder builder = new StringBuilder();

        if (projectsPath == null) {
            projectsPath = config.getConfig().getString(getConfigKey());

            if (!projectsPath.endsWith("/")) {
                projectsPath += "/";
            }
        }

        builder.append(projectsPath);

        return builder;
    }

    protected String zipFolder(File path, String instanceID) throws IOException {

        long startTime = System.currentTimeMillis();

        byte[] buffer = new byte[1024];
        String zipFileName = instanceID + ".zip";

        FileOutputStream tmpZipFile = new FileOutputStream(new File(zipStoragePath, zipFileName));
        ZipOutputStream zos = new ZipOutputStream(tmpZipFile);
        zos.setLevel(Deflater.NO_COMPRESSION);

        List<String> fileList = new ArrayList<String>();
        generateFileList(path.getAbsolutePath(), path, fileList);

        for (String file : fileList) {
            System.out.println("File Added : " + file);
            ZipEntry ze = new ZipEntry(file);
            zos.putNextEntry(ze);

            FileInputStream in = new FileInputStream(path + File.separator + file);

            int len;
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }

            in.close();
        }

        zos.closeEntry();
        zos.close();

        System.out.println(zipFileName);
        logger.warn("ETIME: zip " + (System.currentTimeMillis() - startTime));

        return zipFileName;
    }

    private List<String> generateFileList(String path, File node, List<String> fileList){
        if(node.isFile()){
            fileList.add(generateZipEntry(path, node.getAbsoluteFile().toString()));
        }

        if(node.isDirectory()){
            String[] subNote = node.list();
            for(String filename : subNote){
                generateFileList(path, new File(node, filename), fileList);
            }
        }

        return fileList;
    }

    private String generateZipEntry(String path, String file){
        return file.substring(path.length()+1, file.length());
    }
}
