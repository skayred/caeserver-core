package org.caebeans.storage;

import javax.activation.DataHandler;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/3/12
 * Time: 12:17 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ResultStorageManager {
    File getResultDirectory(String instanceId);

    String getResultPackage(String instanceId) throws IOException;

    void removeResults(String id) throws IOException;
}
