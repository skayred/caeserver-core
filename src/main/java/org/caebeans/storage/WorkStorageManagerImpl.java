package org.caebeans.storage;

import com.google.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.log4j.Logger;
import org.caebeans.caeserver.ProblemCaebean;
import org.caebeans.lib.Config;
import org.caebeans.lib.LoggerManager;
import org.caebeans.lib.Param;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/3/12
 * Time: 3:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class WorkStorageManagerImpl extends StorageManager implements WorkStorageManager {
    public static final String STORAGE_LOCATION_KEY = "storage.work";

    @Inject
    ProjectStorageManager storageManager;

    @Inject
    ResultStorageManager resultStorageManager;

    @Inject
    LoggerManager logger;

    private JAXBContext contextObj;

    @Inject
    WorkStorageManagerImpl(Config config) {
        super(config);
        try {
            contextObj = JAXBContext.newInstance(ProblemCaebean.class);
        } catch (Exception e) {
            logger.error("Cannot create context for marshaller", e);
        }
    }

    @Override
    public void saveProblemCAEBean(String projectId, String instanceId, ProblemCaebean document, Logger instanceLogger) throws IOException {
        if (contextObj != null) {
            StringBuilder problemXmlBuilder = getBuilder();
            problemXmlBuilder
                    .append(instanceId)
                    .append("/")
                    .append(projectId)
                    .append(".xml");

            try {
                Marshaller marshallerObj = contextObj.createMarshaller();
                marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

                marshallerObj.marshal(document, new FileOutputStream(problemXmlBuilder.toString()));
            } catch (Exception e) {
                logger.error("Marshallize XML failed", e);
                instanceLogger.error("Marshallize XML failed", e);
            }
        } else {
            throw new NullPointerException("Context for marshaller is null");
        }
    }

    @Override
    public File getProblemCAEBean(String projectId, String instanceId) {
        logger.info("Getting problem CAEBean for project " + projectId);

        StringBuilder problemXmlBuilder = getBuilder();

        problemXmlBuilder
                .append(instanceId)
                .append("/")
                .append(projectId)
                .append(".xml");

        return new File(problemXmlBuilder.toString());
    }

    @Override
    public void createWorkingDirectory(String projectId, String instanceId) {
        logger.error("Working directory creation for instance " + instanceId + " and project " + projectId);

        StringBuilder taskDirectoryBuilder = getBuilder();
        taskDirectoryBuilder.append(instanceId);

        logger.error("Copying from " + storageManager.getProjectDirectory(projectId).toString() + " to " + taskDirectoryBuilder.toString());

        File taskDirectory = new File(taskDirectoryBuilder.toString());

        try {
            taskDirectory.mkdir();
            FileUtils.copyDirectory(storageManager.getProjectDirectory(projectId), taskDirectory);
        } catch (Exception e) {
            logger.error("Working directory creation", e);
        }
    }

    @Override
    public void deleteWorkingDirectory(String instanceId, Set<String> outputs) {
        logger.info("Working directory removing for instane " + instanceId);

        StringBuilder taskDirectoryBuilder = getBuilder();
        taskDirectoryBuilder.append(instanceId);

        File taskDirectory = new File(taskDirectoryBuilder.toString());
        File resultDirectory = resultStorageManager.getResultDirectory(instanceId);

        WildcardFileFilter filter = new WildcardFileFilter(outputs.toArray(new String[0]));

        System.out.println("Trying to remove working directory");

        try {
            FileUtils.copyDirectory(taskDirectory, resultDirectory, filter);
            FileUtils.deleteDirectory(taskDirectory);
        } catch (Exception e) {
            logger.error("Working directory removing failed", e);
        }
    }

    @Override
    public File createNodeDirectory(String instanceId, String nodeName, List<String> inputs, Logger instanceLogger) {
        logger.info("Node directory creation for instance " + instanceId + " and node " + nodeName);
        instanceLogger.info("Node directory creation for instance " + instanceId + " and node " + nodeName);

        StringBuilder nodeDirectoryBuilder = getBuilder();
        nodeDirectoryBuilder
                .append(instanceId)
                .append("/")
                .append(nodeName);

        File nodeDirectory = new File(nodeDirectoryBuilder.toString());

        WildcardFileFilter filter = new WildcardFileFilter(inputs);

        try {
            nodeDirectory.mkdir();
            FileUtils.copyDirectory(getWorkingDirectory(instanceId), nodeDirectory, filter);

            return nodeDirectory;
        } catch (Exception e) {
            logger.error("Node creation error", e);
            instanceLogger.error("Node creation error", e);

            return null;
        }
    }

    @Override
    public void deleteNodeDirectory(String instanceId, String nodeName, List<String> outputs, Logger instanceLogger) {
        logger.info("Node directory removing for instance " + instanceId + " and node " + nodeName);
        instanceLogger.info("Node directory removing for instance " + instanceId + " and node " + nodeName);

        StringBuilder nodeDirectoryBuilder = getBuilder();
        nodeDirectoryBuilder
                .append(instanceId)
                .append("/")
                .append(nodeName);

        File nodeDirectory = new File(nodeDirectoryBuilder.toString());

        WildcardFileFilter filter = new WildcardFileFilter(outputs);

        try {
            FileUtils.copyDirectory(nodeDirectory, getWorkingDirectory(instanceId), filter);
            FileUtils.deleteDirectory(nodeDirectory);
        } catch (Exception e) {
            logger.error("Node creation error", e);
            instanceLogger.error("Node creation error", e);
        }
    }

    @Override
    public File getWorkingDirectory(String instanceID) {
        StringBuilder taskDirectoryBuilder = getBuilder();
        taskDirectoryBuilder.append(instanceID);

        return new File(taskDirectoryBuilder.toString());
    }

    @Override
    public String getPreprocess(String instanceId, String execName) {
        StringBuilder taskDirectoryBuilder = getBuilder();
        taskDirectoryBuilder
                .append(instanceId)
                .append("/")
                .append(execName)
                .append(".jar");

        return taskDirectoryBuilder.toString();
    }

    @Override
    public Map<String, Param> getProblemParameters(String projectId, String instanceId) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        File problemCaebean = getProblemCAEBean(projectId, instanceId);
        Document problem = dBuilder.parse(problemCaebean);
        problem.getDocumentElement().normalize();

        NodeList categories = problem.getElementsByTagName("category");

        Map<String, Param> params = new HashMap<String, Param>();

        for (int i = 0; i < categories.getLength(); i++) {
            Node category = categories.item(i);

            if (category.getNodeType() == Node.ELEMENT_NODE) {
                NodeList parameters = ((Element) category).getElementsByTagName("parameter");

                for (int j = 0; j < parameters.getLength(); j++) {
                    Node param = parameters.item(j);

                    if (param.getNodeType() == Node.ELEMENT_NODE) {
                        Element parameter = (Element) param;

                        params.put(parameter.getAttribute("name"), getParameter(parameter));
                    }
                }
            }
        }

        return params;
    }

    @Override
    public String getWorkPackage(String instanceID) throws IOException {
        return zipFolder(getWorkingDirectory(instanceID), instanceID);
    }

    private Param getParameter(Element paramNode) {
        int type = Param.TYPES.get(paramNode.getAttribute("type"));
        String value;

        try {
            value = getTagValue("value", paramNode);
        } catch (NullPointerException ne) {
            value = getTagValue("default", paramNode);
        }

        Param param = new Param(type, value);

        return param;
    }

    private String getTagValue(String tag, Element element) {
        NodeList nlList = element.getElementsByTagName(tag).item(0).getChildNodes();

        Node nValue = nlList.item(0);

        return nValue.getNodeValue();
    }

    @Override
    protected String getConfigKey() {
        return STORAGE_LOCATION_KEY;
    }

    @Override
    public void updateProjectFiles(String instanceID, DataHandler projectContent) {
        logger.info("Updating instance " + instanceID);

        File projectsStorage = getWorkingDirectory(instanceID);

        try {
            BufferedOutputStream destination;
            InputStream inputStream = projectContent.getInputStream();
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(inputStream));

            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                String[] nameSegments = entry.getName().split("/");

                if ((entry.getName().contains("__MACOSX")) || (nameSegments[nameSegments.length - 1].startsWith("."))) {
                    continue;
                }

                if (entry.isDirectory()) {
                    logger.info("Extracting directory: " + entry.getName());
                    (new File(projectsStorage, entry.getName())).mkdir();
                    continue;
                }

                logger.info("Extracting: " + entry);

                int count;
                byte data[] = new byte[BUFFER];
                FileOutputStream fos = new FileOutputStream(new File(projectsStorage, entry.getName()));
                destination = new BufferedOutputStream(fos, BUFFER);

                while ((count = zis.read(data, 0, BUFFER)) != -1) {
                    destination.write(data, 0, count);
                }

                destination.flush();
                destination.close();
            }

            zis.close();
        } catch (IOException e) {
            logger.fatal("Failed to deploy project", e);
        }

        logger.info("Project updated. Indexing...");
    }
}
