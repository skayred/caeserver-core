package org.caebeans.broker.client;

import de.fzj.unicore.wsrflite.xmlbeans.client.BaseWSRFClient;
import eu.unicore.security.util.Log;
import eu.unicore.security.util.client.IClientProperties;
import org.apache.log4j.Logger;
import org.caebeans.xmlbeans.services.bs.CreateBRDocument;
import org.caebeans.xmlbeans.services.bs.CreateBRResponseDocument;
import org.caebeans.xmlbeans.services.bs.DestroyBRDocument;
import org.caebeans.xmlbeans.services.bs.DestroyBRResponseDocument;
import org.oasisOpen.docs.wsrf.rl2.TerminationTimeDocument.TerminationTime;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.util.Calendar;

/**
 * Client for the TargetSystemFactory service
 *
 * @author
 */
public class BSClient extends BaseWSRFClient {

    private static final Logger logger = Log.getLogger(Log.CLIENT, BSClient.class);

    //private final TargetSystemFactory tsf;
    private final BrokerIFace bs;

    public BSClient(String endpointUrl, EndpointReferenceType epr, IClientProperties sec) throws Exception {
        super(endpointUrl, epr, sec);
        bs = makeProxy(BrokerIFace.class);
    }

    public BSClient(EndpointReferenceType epr, IClientProperties sec) throws Exception {
        this(epr.getAddress().getStringValue(), epr, sec);
    }

    public BWClient createBW(CreateBRDocument in) throws Exception {
        logger.info("Calling a broker service at: " + getEPR().getAddress().getStringValue());
        CreateBRResponseDocument res = bs.CreateBrokerResource(in);
        EndpointReferenceType epr = res.getCreateBRResponse().getBrReference();
        return new BWClient(epr.getAddress().getStringValue(), epr, securityProperties);
    }

    /**
     * create a TSS with the supplied initial termination time
     *
     * @param initialTerminationTime
     * @return
     * @throws de.fzj.unicore.wsrflite.xmlbeans.BaseFault
     *
     */
    public BWClient createBW(Calendar initialTerminationTime) throws Exception {
        CreateBRDocument in = CreateBRDocument.Factory.newInstance();
        TerminationTime tt = TerminationTime.Factory.newInstance();
        tt.setCalendarValue(initialTerminationTime);
        in.addNewCreateBR().setTerminationTime(tt);
        return createBW(in);
    }

    public BWClient createBW() throws Exception {
        CreateBRDocument in = CreateBRDocument.Factory.newInstance();
        in.addNewCreateBR();
        return createBW(in);
    }

    public void destroyBW(DestroyBRDocument in) throws Exception {
        DestroyBRResponseDocument res = bs.DestroyBrokerResource(in);
    }

    /**
     * returns the service's TargetSystemPropertiesDocument
     * @param endpointUrl
     * @param epr
     */    /*
	public BrokerPropertiesDocument getResourcePropertiesDocument()throws Exception{
		return BrokerPropertiesDocument.Factory.parse(GetResourcePropertyDocument().getGetResourcePropertyDocumentResponse().newInputStream());
	}*/
	/*
	public ConcreteWorkflow getNewCWF(){
		try{
			ConcreteWorkflow cwf = getResourcePropertiesDocument().getBrokerProperties().getConcreteWorkflow();
			return cwf;
		}catch(Exception e){
			logger.error("Can't get ConcreteWorkflow.",e);
		}
		return null;
	}*/
	/*
	public boolean supportsReservation()throws Exception{
		return getResourcePropertiesDocument().getBrokerProperties().getSupportsReservation();
	}*/
	/*
	public boolean supportsVirtualImages()throws Exception{
		return getResourcePropertiesDocument().getBrokerProperties().getSupportsVirtualImages();
	}*/
	/*
	public List<EndpointReferenceType> getTargetSystems(){
		try{
			EndpointReferenceType[] eprs=getResourcePropertiesDocument().getBrokerProperties().
			getTargetSystemReferenceArray();
			return Arrays.asList(eprs);
		}catch(Exception e){
			logger.error("Can't get target system list.",e);
		}
		return null;
	}*/

	/*
	public ConcreteWorkflow getConcreteWorkflow() throws Exception{
		return getResourcePropertiesDocument().getBrokerProperties().getConcreteWorkflow();
	}*/

    /**
     * get the user's logins on the remote machine
     * @return String[] or null if an error occured
     */
	/*
	public String[] getXlogins(){
		try{
			String[] srs=getResourcePropertiesDocument().getTargetSystemFactoryProperties().getXloginArray();
			return srs;
		}catch(Exception e){
			logger.error("Can't get xlogins.",e);
		}
		return null;
	}*/

    /**
     * get the user's groups on the remote machine
     * @return String[] or null if an error occured
     */
	/*
	public String[] getXgroups(){
		try{
			String[] srs=getResourcePropertiesDocument().getTargetSystemFactoryProperties().getXgroupArray();
			return srs;
		}catch(Exception e){
			logger.error("Can't get xgroups.",e);
		}
		return null;
	}*/

    /**
     * gets the TSSs that are accessible for this client
     *
     * @return accessible TSS addresses
     * @since 1.1.2
     */
	/*
	public List<EndpointReferenceType> getAccessibleTargetSystems(){
		try{
			EndpointReferenceType[] eprs=getResourcePropertiesDocument().getTargetSystemFactoryProperties().
			getAccessibleTargetSystemReferenceArray();
			return Arrays.asList(eprs);
		}catch(Exception e){
			logger.error("Can't get accessible target system list.",e);
		}
		return null;
	}*/

}
