package org.caebeans.broker.client;


import de.fzj.unicore.uas.TargetSystem;
import de.fzj.unicore.uas.TargetSystemFactory;
import de.fzj.unicore.uas.client.ReservationClient;
import de.fzj.unicore.wsrflite.xmlbeans.client.BaseWSRFClient;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import eu.unicore.security.util.Log;
import eu.unicore.security.util.client.IClientProperties;
import org.apache.log4j.Logger;
import org.caebeans.xmlbeans.services.bwf.GetWFDocument;
import org.caebeans.xmlbeans.services.bwf.GetWFResponseDocument;
import org.caebeans.xmlbeans.services.bwf.StatusDocument;
import org.caebeans.xmlbeans.services.bwf.TStatus;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ResourcesDocument;
import org.unigrids.x2006.x04.services.reservation.ResourceReservationRequestDocument;
import org.unigrids.x2006.x04.services.reservation.ResourceReservationResponseDocument;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.util.Calendar;
import java.util.List;

public class BWClient extends BaseWSRFClient {

	private static final Logger logger= Log.getLogger(Log.CLIENT, BWClient.class);

	private final BrokeredWorkflowIFace bw;
	
	private String name;

	public BWClient(String endpointUrl, EndpointReferenceType epr, IClientProperties sec)throws Exception {
		super(endpointUrl, epr,sec);
		bw = makeProxy(BrokeredWorkflowIFace.class);
	}

	public BWClient(EndpointReferenceType epr, IClientProperties sec)throws Exception {
		this(epr.getAddress().getStringValue(), epr,sec);
	}
	
	public GetWFResponseDocument GetWorkflow(GetWFDocument request) {
		GetWFResponseDocument res = bw.GetWorkflow(request);
		return res;
	}

	public StatusDocument getResourcePropertiesDocument()throws Exception{
		return StatusDocument.Factory.parse(GetResourcePropertyDocument().getGetResourcePropertyDocumentResponse().newInputStream());
	}
	
	public TStatus getStatus(){
		try{
			TStatus status = getResourcePropertiesDocument().getStatus();
			return status;
		}catch(Exception e){
			logger.error("Can't get Status.",e);
		}
		return null;
	}

	/**
	 * submit a job
	 */
	/*
	public SubmitResponseDocument Submit(SubmitDocument in) throws Exception, AutoStartNotSupportedException{
		if(logger.isDebugEnabled())logger.debug("Calling target system service at "+getEPR().getAddress().getStringValue());
		boolean autoStart=in.getSubmit().getAutoStartWhenReady();
		if(autoStart && !checkVersion("1.4.1")){
			throw new AutoStartNotSupportedException();
		}
		return tss.Submit(in);
	}*/

	/**
	 * Submit a job, and return a JMS client
	 * @param in
	 * @return
	 * @throws Exception
	 */
	/*
	public JobClient submit(SubmitDocument in) throws Exception {
		SubmitResponseDocument res=Submit(in);
		EndpointReferenceType epr=res.getSubmitResponse().getJobReference();
		return new JobClient(epr.getAddress().getStringValue(), epr, securityProperties);
	}*/


	/**
	 * returns the service's TargetSystemPropertiesDocument
	 */
	/*
	public StatusDocument getResourcePropertiesDocument()throws Exception{
		return StatusDocument.Factory.parse(GetResourcePropertyDocument().getGetResourcePropertyDocumentResponse());
	}*/

	/**
	 * return the service's UpSince property
	 * @return Calendar
	 * @since 1.0.1
	 */
	/*
	public Calendar getUpSince()throws Exception{
		return getResourcePropertiesDocument().getWorkflowProperties().?.getUpSince();
	}*/

	/**
	 * return the number of jobs on the resource
	 * @return the number of jobs
	 * @since 1.0.1
	 */
	/*
	public long getNumberOfJobs()throws Exception{
		return getResourcePropertiesDocument().getTargetSystemProperties().getTotalNumberOfJobs().longValue();
	}*/

	/**
	 * get the current list of jobs on this target system
	 * @return List of job eprs
	 */
	/*
	private List<EndpointReferenceType> getJobsWithoutEnumeration()throws Exception{
		EndpointReferenceType[] eprs=getResourcePropertiesDocument().getTargetSystemProperties().getJobReferenceArray();
		return Arrays.asList(eprs);
	}*/

	/**
	 * returns the list of job references.br/>
	 * To handle very large lists, you should use {@link #getJobReferenceEnumeration()}
	 * instead. 
	 * Note that that method returns null in case you're accessing an
	 * older server.
	 * @return
	 * @throws Exception
	 */
	/*
	public List<EndpointReferenceType> getJobs()throws Exception{
		EnumerationClient<JobReferenceDocument>c=getJobReferenceEnumeration();
		if(c==null)return getJobsWithoutEnumeration();
		List<EndpointReferenceType>res=new ArrayList<EndpointReferenceType>();
		Iterator<JobReferenceDocument>iter=c.iterator();
		while(iter.hasNext()){
			res.add(iter.next().getJobReference());
		}
		return res;
	}*/

	/**
	 * @since 6.3.0
	 * @return an {@link EnumerationClient} for accessing the job list, or null in case of
	 * an older server that does not support enumerations
	 */
	/*
	public EnumerationClient<JobReferenceDocument>getJobReferenceEnumeration()throws Exception{
		EndpointReferenceType epr=getResourcePropertiesDocument().getTargetSystemProperties().getJobReferenceEnumeration();
		if(epr!=null){
			EnumerationClient<JobReferenceDocument>c=new EnumerationClient<JobReferenceDocument>(epr,securityProperties,JobReferenceDocument.type.getDocumentElementName());
			return c;
		}
		return null;
	}*/
	/**
	 * get the current list of reservations
	 * @return List of job eprs
	 */
	/*
	public List<EndpointReferenceType> getReservations(){
		try{
			EndpointReferenceType[] eprs=getResourcePropertiesDocument().getTargetSystemProperties().getReservationReferenceArray();
			return Arrays.asList(eprs);
		}catch(Exception e){
			logger.error("Can't get reservation list.",e);
		}
		return null;
	}*/

	/**
	 * get the current list of storages attached to this target system
	 * @return List of storage eprs
	 */
	/*
	public List<EndpointReferenceType> getStorages(){
		try{
			StorageReferenceType[] srs=getResourcePropertiesDocument().getTargetSystemProperties().getStorageReferenceArray();
			List<EndpointReferenceType>eprs=new ArrayList<EndpointReferenceType>();
			for(StorageReferenceType s: srs)eprs.add(s.getStorageEndpointReference());
			return eprs;
		}catch(Exception e){
			logger.error("Can't get storages list.",e);
		}
		return null;
	}*/

	/**
	 * get the user's logins on the remote machine
	 * @return String[] or null if an error occured
	 */
	/*
	public String[] getXlogins(){
		try{
			String[] srs=getResourcePropertiesDocument().getTargetSystemProperties().getXloginArray();
			return srs;
		}catch(Exception e){
			logger.error("Can't get xlogins.",e);
		}
		return null;
	}*/

	/**
	 * get the user's groups on the remote machine
	 * @return String[] or null if an error occured
	 */
	/*
	public String[] getXgroups(){
		try{
			String[] srs=getResourcePropertiesDocument().getTargetSystemProperties().getXgroupArray();
			return srs;
		}catch(Exception e){
			logger.error("Can't get xgroups.",e);
		}
		return null;
	}*/

	/**
	 * get the "Name" of this target system
	 * @return
	 */
	/*
	public String getTargetSystemName(){
		if(name==null){
			try{
				name=getResourcePropertiesDocument().getTargetSystemProperties().getName();
			}catch(Exception e){
				logger.warn("Can't get name from TSS.",e);
			}
		}
		return name;
	}*/

	/**
	 * get the list of applications installed on the targetsystem
	 * @return List of applications
	 */
	/*
	public List<ApplicationResourceType> getApplications(){
		try{
			ApplicationResourceType[] apps=getResourcePropertiesDocument().getTargetSystemProperties().getApplicationResourceArray();
			return Arrays.asList(apps);
		}catch(Exception e){
			logger.warn("Can't get applications from TSS.",e);
		}
		return null;
	}*/


	/**
	 * get the list of TextInfo for this TSS
	 * @return List of TextInfo
	 */
	/*
	public List<TextInfoType> getTextInfo(){
		try{
			TextInfoType[] info=getResourcePropertiesDocument().getTargetSystemProperties().getTextInfoArray();
			return Arrays.asList(info);
		}catch(Exception e){
			logger.warn("Can't get text info from TSS.",e);
		}
		return null;
	}*/

	/**
	 * Returns site specific resources --- new servers will rather use AvailableResources 
	 * @return the list of site specific resources
	 * @since 1.0.1
	 */
	/*
	public List<SiteResourceType> getSiteSpecificResources(){
		try{
			SiteResourceType[] info = getResourcePropertiesDocument().getTargetSystemProperties().getSiteResourceArray();
			return Arrays.asList(info);
		}catch(Exception e){
			logger.warn("Can't get site-specific resource info from TSS.",e);
		}
		return null;
	}*/
	
	/**
	 * returns (non-jsdl) resources that are available on the tss 
	 * @return
	 * @since 1.4.0
	 */
	/*
	public List<AvailableResourceType> getAvailableResources(){
		try{
			AvailableResourceType[] info = getResourcePropertiesDocument().getTargetSystemProperties().getAvailableResourceArray();
			return Arrays.asList(info);
		}catch(Exception e){
			logger.warn("Can't get resource info from TSS.",e);
		}
		return null;
	}*/
/*
	public String getOperatingSystemInfo() throws Exception{
		OperatingSystemType os=getResourcePropertiesDocument().getTargetSystemProperties().getOperatingSystem();
		if(os==null)return "UNKNOWN";
		try{
			String name=os.getOperatingSystemType().getOperatingSystemName().toString();
			String version=os.getOperatingSystemVersion();
			String descrString=os.getDescription();
			return name+" "+ ( version!=null? version: "") + (descrString!=null?" ("+descrString+")": "");
		}catch(Exception e){
			logger.warn("Error retrieving OS from target system.");
			return "UNKNOWN";
		}
	}*/
/*
	public OperatingSystemType getOperatingSystem() throws Exception{
		OperatingSystemType os=getResourcePropertiesDocument().getTargetSystemProperties().getOperatingSystem();
		return os;
	}*/

	/**
	 * helper for retrieving a TSSClient from a given registry 
	 * @param registryURL
	 * @return TSSClient
	 */
	public static BWClient getOrCreateBW(String registryURL, IClientProperties sec)throws Exception{
		EndpointReferenceType epr= EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(registryURL);
		RegistryClient c=new RegistryClient(registryURL,epr,sec);
		List<EndpointReferenceType> bw = c.listAccessibleServices(BrokeredWorkflowIFace.BWF_PORT);
		if(bw.size()==0){
			return createBW(c,sec);
		}
		else if(bw.get(0).getAddress().getStringValue().contains("WorkflowTargetSystem")){
			if(bw.size()>0){
				return new BWClient(bw.get(1).getAddress().getStringValue(),bw.get(1),sec);
			}
			else return createBW(c,sec);
		}
		else
		{
			return new BWClient(bw.get(0).getAddress().getStringValue(),bw.get(0),sec);
		}
	} 

	private static BWClient createBW(RegistryClient c,IClientProperties sec)throws Exception{
		List<EndpointReferenceType> bs =c.listAccessibleServices(TargetSystemFactory.TSF_PORT);
		if(bs.size()==0)return null;
		return new BSClient(bs.get(0).getAddress().getStringValue(),bs.get(0),sec).createBW();
	}

	/**
	 * does this target system support reservation?
	 * 
	 * @return true if the TSS supports reservation
	 * @throws Exception
	 */
	/*
	public boolean supportsReservation()throws Exception{
		SupportsReservationDocument resSupport=SupportsReservationDocument.Factory.parse(getResourceProperty(ResourceReservation.RP_SUPPORTS_RESERVATION));
		return resSupport.getSupportsReservation();
	}*/

	/**
	 * create a reservation
	 * TODO make a client for managing the reservation (and submitting?)
	 * 
	 * @return {@link org.w3.x2005.x08.addressing.EndpointReferenceType} of the new reservation
	 * @throws Exception
	 */
	public EndpointReferenceType createReservation(ResourcesDocument resources, Calendar startTime)throws Exception{
		ResourceReservationRequestDocument req= ResourceReservationRequestDocument.Factory.newInstance();
		req.addNewResourceReservationRequest().setResources(resources.getResources());
		req.getResourceReservationRequest().setStartTime(startTime);
		TargetSystem tss=makeProxy(TargetSystem.class);
		ResourceReservationResponseDocument res=tss.ReserveResources(req);
		return res.getResourceReservationResponse().getReservationReference();
	}
	/**
	 * create a client for managing the reservation
	 * 
	 * @return BaseUASClient
	 * @throws Exception
	 */
	public ReservationClient createReservationClient(ResourcesDocument resources, Calendar startTime)throws Exception{
		EndpointReferenceType epr=createReservation(resources, startTime);
		ReservationClient client=new ReservationClient(epr.getAddress().getStringValue(),epr,securityProperties);
		return client;
	}

	
}
