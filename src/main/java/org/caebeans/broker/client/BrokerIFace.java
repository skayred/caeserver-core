
package org.caebeans.broker.client;

import de.fzj.unicore.uas.ResourceReservation;
import de.fzj.unicore.wsrflite.utils.deployment.DeploymentDescriptor;
import de.fzj.unicore.wsrflite.xfire.XFireService;
import de.fzj.unicore.wsrflite.xmlbeans.BaseFault;
import de.fzj.unicore.wsrflite.xmlbeans.ResourceLifetime;
import de.fzj.unicore.wsrflite.xmlbeans.ResourceProperties;
import eu.unicore.security.xfireutil.RequiresSignature;
import org.caebeans.xmlbeans.services.bs.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.namespace.QName;

/**
 * Service interface definition. Methods that should be accessible through
 * SOAP are marked with the WebMethod annotation. The
 * whole interface is marked with the WebService annotation.
 */
@WebService(targetNamespace = "http://caebeans.org/services/bs", portName = "Broker")
@DeploymentDescriptor(iFace="org.caebeans.services.BrokerIFace", impl="org.caebeans.services.BrokerHomeImpl", isPersistent=true, type= XFireService.TYPE)
public interface BrokerIFace extends ResourceReservation, ResourceProperties, ResourceLifetime {

	// namespace
	public static final String BS_NS = "http://caebeans.org/services/bs";
	
	// name of a service
	public static final String NAME = "Broker";
	
	// porttype
	public static final QName BS_PORT = new QName(BS_NS, NAME);
	
	// a default name for a instance of a broker service
	public static final String DEFAULT_BS = "default_broker";
	
	// action for "CreateBR"
	public static final String ACTION_CREATEBR="http://caebeans.org/services/bs/CreateBR";

	// action for "DestroyBR"
	public static final String ACTION_DESTROYBR="http://caebeans.org/services/bs/DestroyBR";

	// resource property QNames
	public static final QName RPBSReferencesResource = BSReferenceResourceDocument.type.getDocumentElementName();
	
	/**
	 * method to create a WSRFResource of a brokered workflow
	 */
	@RequiresSignature
	@WebMethod(action = ACTION_CREATEBR)
	public CreateBRResponseDocument CreateBrokerResource(CreateBRDocument request) throws BaseFault;

	/**
	 * method to destroy a WSRFResource of a brokered workflow
	 */
	@RequiresSignature
	@WebMethod(action = ACTION_DESTROYBR)
	public DestroyBRResponseDocument DestroyBrokerResource(DestroyBRDocument request) throws BaseFault;

}
