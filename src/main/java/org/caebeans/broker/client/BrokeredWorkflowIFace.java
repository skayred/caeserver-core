package org.caebeans.broker.client;

import de.fzj.unicore.wsrflite.xmlbeans.ResourceLifetime;
import de.fzj.unicore.wsrflite.xmlbeans.ResourceProperties;
import org.caebeans.xmlbeans.services.bwf.GetWFDocument;
import org.caebeans.xmlbeans.services.bwf.GetWFResponseDocument;
import org.caebeans.xmlbeans.services.bwf.StatusDocument;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.namespace.QName;

@WebService(targetNamespace = "http://caebeans.org/services/bwf", portName="BrokeredWorkflow")
public interface BrokeredWorkflowIFace extends ResourceProperties,ResourceLifetime {
	// namespace
	public static final String BWF_NS = "http://caebeans.org/services/bwf";

	// name of a service
	public static final String NAME = "BrokeredWorkflow";
	
	// porttype
	public static final QName BWF_PORT = new QName(BWF_NS,NAME);
	
	// action for "GetWF"
	public static final String ACTION_GETWF = "http://caebeans.org/services/bwf/GetWF";
	
	// resource property QNames
	public static final QName RPStatus = StatusDocument.type.getDocumentElementName();

	/**
	 * method to create a WSRFResource of a broker service
	 */
	@WebMethod(action = ACTION_GETWF)
	public GetWFResponseDocument GetWorkflow(GetWFDocument request);
}
