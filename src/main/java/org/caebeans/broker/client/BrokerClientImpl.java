package org.caebeans.broker.client;

import com.google.inject.Inject;
import de.fzj.unicore.uas.security.ClientProperties;
import org.apache.commons.configuration.Configuration;
import org.caebeans.lib.Config;
import org.caebeans.lib.LoggerManager;
import org.caebeans.xmlbeans.scheduler.awf.AbstractWorkflowDocument;
import org.caebeans.xmlbeans.scheduler.cwf.ConcreteWorkflowDocument;
import org.caebeans.xmlbeans.services.bs.CreateBRDocument;
import org.caebeans.xmlbeans.services.bs.DestroyBRDocument;
import org.caebeans.xmlbeans.services.bwf.GetWFDocument;
import org.caebeans.xmlbeans.services.bwf.GetWFResponseDocument;
import org.oasisOpen.docs.wsrf.rl2.TerminationTimeDocument;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/15/12
 * Time: 9:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class BrokerClientImpl implements BrokerClient {
    private static final String BROKER_URL = "broker.url";
    private static final String SSL_KEYSTORE = "wsrf.ssl.keystore";
    private static final String SSL_KEYPASS = "wsrf.ssl.keypass";
    private static final String SSL_KEYALIAS = "wsrf.ssl.keyalias";
    private static final String SSL_TRUSTSTORE = "wsrf.ssl.truststore";
    private static final String SSL_TRUSTPASS = "wsrf.ssl.trustpass";

    @Inject
    private LoggerManager logger;

    private BSClient serviceClient;
    private BWClient resourceClient;

    @Inject
    BrokerClientImpl(Config config, LoggerManager logger) {
        Configuration conf = config.getConfig();

        EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
        String baseurl = conf.getString(BROKER_URL);
        String uri = baseurl + "/" + BrokerIFace.NAME + "?res=" + BrokerIFace.DEFAULT_BS;

        epr.addNewAddress().setStringValue(uri);

        ClientProperties cpr = new ClientProperties();
        cpr.setProperty(ClientProperties.WSRF_SSL, "true");

        cpr.setProperty(ClientProperties.WSRF_SSL_KEYSTORE, conf.getString(SSL_KEYSTORE));
        cpr.setProperty(ClientProperties.WSRF_SSL_KEYPASS, conf.getString(SSL_KEYPASS));
        cpr.setProperty(ClientProperties.WSRF_SSL_KEYALIAS, conf.getString(SSL_KEYALIAS));
        cpr.setProperty(ClientProperties.WSRF_SSL_TRUSTSTORE, conf.getString(SSL_TRUSTSTORE));
        cpr.setProperty(ClientProperties.WSRF_SSL_TRUSTPASS, conf.getString(SSL_TRUSTPASS));

        try {
            serviceClient = new BSClient(epr, cpr);
        } catch (Exception e) {
            logger.fatal("Failed to create broker client", e);
        }
        System.out.println(epr.toString());
    }

    @Override
    public ConcreteWorkflowDocument.ConcreteWorkflow seachTSS(AbstractWorkflowDocument.AbstractWorkflow abstractWorkflow) throws Exception {
        if (serviceClient == null) {
            throw new Exception("Broker client is null. See log for more details");
        }

        CreateBRDocument request = CreateBRDocument.Factory.newInstance();
        CreateBRDocument.CreateBR br = request.addNewCreateBR();
        br.setAbstractWorkflow(abstractWorkflow);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 10);
        TerminationTimeDocument.TerminationTime tt = TerminationTimeDocument.TerminationTime.Factory.newInstance();
        tt.setCalendarValue(c);
        br.setTerminationTime(tt);

        resourceClient = serviceClient.createBW(request);

        GetWFDocument req = GetWFDocument.Factory.newInstance();
        req.addNewGetWF();
        GetWFResponseDocument concreteWorkflow = resourceClient.GetWorkflow(req);

        DestroyBRDocument in = DestroyBRDocument.Factory.newInstance();
        in.addNewDestroyBR();
        in.getDestroyBR().setBrReference(resourceClient.getEPR());

        serviceClient.destroyBW(in);

        return concreteWorkflow.getGetWFResponse().getConcreteWorkflow();
    }
}
