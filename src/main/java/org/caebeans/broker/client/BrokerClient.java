package org.caebeans.broker.client;

import org.caebeans.xmlbeans.scheduler.awf.AbstractWorkflowDocument;
import org.caebeans.xmlbeans.scheduler.cwf.ConcreteWorkflowDocument;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 10/16/12
 * Time: 7:22 PM
 * To change this template use File | Settings | File Templates.
 */
public interface BrokerClient {
    ConcreteWorkflowDocument.ConcreteWorkflow seachTSS(AbstractWorkflowDocument.AbstractWorkflow abstractWorkflow) throws Exception;
}
