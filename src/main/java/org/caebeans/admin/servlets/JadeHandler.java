package org.caebeans.admin.servlets;

import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/17/12
 * Time: 8:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class JadeHandler extends AbstractHandler {
    private String jades;

    public JadeHandler(String[] files) {
        jades = createTemplates(files);
    }

    private String createTemplates(String[] files) {
        StringBuilder result = new StringBuilder();
        Map<String, String> jades = new HashMap<String, String>();

        for (String file : files) {
            try {
                jades.put(file, getContent(file));
            } catch (IOException e) {
                //  Do nothing
            }
        }

        result.append("var JST = new Array();\n\n");

        for (Map.Entry<String, String> jade : jades.entrySet()) {
            result.append("JST[\"" + jade.getKey().replace(".jade", "")
                    + "\"] = jade.compile(\""
                    + jade.getValue().replace("\"", "'").replace("\n", "\\n").replace("\t", "\\t") + "\");");
            result.append("\n");
        }

        return result.toString();
    }

    private String getContent(String filename) throws IOException {
        InputStream stream = getClass().getClassLoader().getResourceAsStream("admin/web/templates/" + filename);

        System.out.println(stream);

        StringWriter writer = new StringWriter();
        IOUtils.copy(stream, writer);
        return writer.toString();
    }

    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setContentType("text/javascript;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);

        response.getWriter().println(jades);
    }
}
