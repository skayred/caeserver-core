package org.caebeans.admin;

import org.caebeans.admin.api.ApiApplication;
import org.caebeans.admin.servlets.JadeHandler;
import org.caebeans.admin.servlets.TransmissionServlet;
import org.caebeans.lib.Config;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.SecurityHandler;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.MultiPartFilter;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.security.Credential;
import org.restlet.ext.servlet.ServerServlet;

import javax.servlet.DispatcherType;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/20/12
 * Time: 9:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdminServerInitializer {
    public static final String ADMIN_PORT = "admin.port";
    public static final String RESULT_ZIP_DIRECTORY = "storage.results_zip_dir";

    public static void initAdminServer(Config config) throws Exception {
        Server server = new Server(config.getConfig().getInt(ADMIN_PORT));

        String webDir = AdminServerInitializer.class.getClassLoader().getResource("admin/web").toExternalForm();

        /*************/

        ContextHandler staticContext = new ContextHandler();
        staticContext.setContextPath("/");
        staticContext.setResourceBase(".");
        staticContext.setClassLoader(Thread.currentThread().getContextClassLoader());

        ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setDirectoriesListed(true);
        resourceHandler.setWelcomeFiles(new String[]{"index.html"});
        resourceHandler.setResourceBase(webDir);

        staticContext.setHandler(resourceHandler);

        ResourceHandler resultZipsHandler = new ResourceHandler();
        resourceHandler.setResourceBase(config.getConfig().getString(RESULT_ZIP_DIRECTORY));

        ContextHandler resultStaticContext = new ContextHandler();
        staticContext.setContextPath("/results");
        staticContext.setResourceBase(".");
        staticContext.setClassLoader(Thread.currentThread().getContextClassLoader());


        ServletContextHandler projectsUploadContext = new ServletContextHandler();
        projectsUploadContext.setContextPath("/");
        projectsUploadContext.setResourceBase(".");
        projectsUploadContext.setClassLoader(Thread.currentThread().getContextClassLoader());
        projectsUploadContext.addServlet(new ServletHolder(new TransmissionServlet()), "/projects");

//        projectsUploadContext.addFilter(MultiPartFilter.class, "/", null);

        /******************/

        ContextHandler jadeContext = new ContextHandler();
        jadeContext.setContextPath("/jade");
        jadeContext.setResourceBase(".");
        jadeContext.setClassLoader(Thread.currentThread().getContextClassLoader());
        server.setHandler(jadeContext);

        jadeContext.setHandler(new JadeHandler(new String[]{
                "admin/projects.jade",
                "admin/project_item.jade",
                "admin/project_details.jade",
                "admin/node_details.jade",
                "admin/logs.jade",
                "admin/log_item.jade",
                "admin/log_details.jade"
        }));

        /*******************/

        ServletContextHandler restletContext = new ServletContextHandler(ServletContextHandler.SESSIONS);
        restletContext.setContextPath("/api");

        restletContext.setSecurityHandler(basicAuth("admin", "cft6yhnvgy7ujm"));

        ServerServlet serverServlet = new ServerServlet();
        ServletHolder servletHolder = new ServletHolder(serverServlet);
        servletHolder.setInitParameter("org.restlet.application", ApiApplication.class.getCanonicalName());
        restletContext.addServlet(servletHolder, "/*");

        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{staticContext, projectsUploadContext, jadeContext, restletContext});
        server.setHandler(handlers);

        server.start();
        server.join();
    }

    private static final SecurityHandler basicAuth(String username, String password) {
        HashLoginService l = new HashLoginService();
        l.putUser(username, Credential.getCredential(password), new String[]{"user"});
        l.setName("CAEServer");

        Constraint constraint = new Constraint();
        constraint.setName(Constraint.__BASIC_AUTH);
        constraint.setRoles(new String[]{"user"});
        constraint.setAuthenticate(true);

        ConstraintMapping cm = new ConstraintMapping();
        cm.setConstraint(constraint);
        cm.setPathSpec("/*");

        ConstraintSecurityHandler csh = new ConstraintSecurityHandler();
        csh.setAuthenticator(new BasicAuthenticator());
        csh.setRealmName("CAEServer");
        csh.addConstraintMapping(cm);
        csh.setLoginService(l);

        return csh;

    }
}
