package org.caebeans.admin.api;

import com.google.inject.Inject;
import org.caebeans.lib.LogContent;
import org.caebeans.lib.LoggerManager;
import org.restlet.resource.Get;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/20/12
 * Time: 9:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class LogsResource extends InjectServerResource {
    @Inject
    private LoggerManager logger;

    @Get
    public List<LogContent> getProjectsList() {
        try {
            return logger.getLogs();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<LogContent>();
        }
    }
}
