package org.caebeans.admin.api;

import com.google.inject.Inject;
import org.caebeans.database.ProjectDescriptionManager;
import org.caebeans.task.description.TaskDescription;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/20/12
 * Time: 9:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProjectsResource extends InjectServerResource {
    @Inject
    private ProjectDescriptionManager descriptionManager;

    @Get
    public List<TaskDescription> getProjectsList() {
        try {
            return descriptionManager.getAllDescriptions();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<TaskDescription>();
        }
    }
}
