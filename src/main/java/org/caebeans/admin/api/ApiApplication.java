package org.caebeans.admin.api;

import org.caebeans.admin.lib.FixedJacksonConverter;
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.data.MediaType;
import org.restlet.engine.Engine;
import org.restlet.engine.converter.ConverterHelper;
import org.restlet.ext.jackson.JacksonConverter;
import org.restlet.routing.Router;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/21/12
 * Time: 4:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class ApiApplication extends Application {
    @Override
    public Restlet createInboundRoot() {
        Router router = new Router(getContext());

        getMetadataService().setDefaultMediaType(MediaType.APPLICATION_JSON);
        replaceConverter(JacksonConverter.class, new FixedJacksonConverter());

        router.attach("/projects", ProjectsResource.class);
        router.attach("/logs", LogsResource.class);

        return router;
    }

    static void replaceConverter(Class<? extends ConverterHelper> converterClass, ConverterHelper newConverter) {

        ConverterHelper oldConverter = null;
        List<ConverterHelper> converters = Engine.getInstance().getRegisteredConverters();
        for (ConverterHelper converter : converters) {
            if (converter.getClass().equals(converterClass)) {
                converters.remove(converter);
                oldConverter = converter;
                break;
            }
        }

        converters.add(newConverter);
        if (oldConverter == null) {
            System.err.println("Added Converter to Restlet Engine: " + newConverter.getClass().getName());
        } else {
            System.err.println("Replaced Converter " + oldConverter.getClass().getName() + " with " +
                    newConverter.getClass().getName() + " in Restlet Engine");
        }
    }
}
