package org.caebeans.admin.api;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.caebeans.ServerModule;
import org.restlet.resource.ServerResource;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/22/12
 * Time: 11:04 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class InjectServerResource extends ServerResource {
    public InjectServerResource() {
        Injector injector = Guice.createInjector(new ServerModule());
        injector.injectMembers(this);
    }
}
