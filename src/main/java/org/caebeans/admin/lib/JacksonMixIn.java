package org.caebeans.admin.lib;

import com.google.inject.Key;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/21/12
 * Time: 10:20 PM
 * To change this template use File | Settings | File Templates.
 */
public interface JacksonMixIn {
    @JsonIgnore
    <V> Key<V> getRoot();
}
