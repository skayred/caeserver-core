package org.caebeans.admin.lib;

import com.google.inject.Key;
import org.codehaus.jackson.map.ObjectMapper;
import org.restlet.data.MediaType;
import org.restlet.ext.jackson.JacksonRepresentation;
import org.restlet.representation.Representation;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/21/12
 * Time: 10:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class FixedJacksonRepresentation<T> extends JacksonRepresentation<T> {

    public FixedJacksonRepresentation(MediaType mediaType, T object) {
        super(mediaType, object);
    }

    public FixedJacksonRepresentation(T object) {
        super(object);
    }

    public FixedJacksonRepresentation(Representation representation, Class<T> objectClass) {
        super(representation, objectClass);
    }

    @Override
    protected ObjectMapper createObjectMapper() {
        ObjectMapper ret = super.createObjectMapper();

// inject the mixin that will allow us to properly serialize Objectify Key objects...
        ret.getSerializationConfig().addMixInAnnotations(Key.class, JacksonMixIn.class);
        ret.getDeserializationConfig().addMixInAnnotations(Key.class, JacksonMixIn.class);

        return ret;
    }
}
