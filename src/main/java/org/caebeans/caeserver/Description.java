
package org.caebeans.caeserver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://caeserver.caebeans.org}Status"/>
 *         &lt;element ref="{http://caeserver.caebeans.org}TerminationTime"/>
 *         &lt;element ref="{http://caeserver.caebeans.org}CurrentTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "status",
    "terminationTime",
    "currentTime"
})
@XmlRootElement(name = "Description")
public class Description {

    @XmlElement(name = "Status", required = true)
    protected Status status;
    @XmlElement(name = "TerminationTime", required = true, nillable = true)
    protected TerminationTime terminationTime;
    @XmlElement(name = "CurrentTime", required = true)
    protected CurrentTime currentTime;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }

    /**
     * Gets the value of the terminationTime property.
     * 
     * @return
     *     possible object is
     *     {@link TerminationTime }
     *     
     */
    public TerminationTime getTerminationTime() {
        return terminationTime;
    }

    /**
     * Sets the value of the terminationTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link TerminationTime }
     *     
     */
    public void setTerminationTime(TerminationTime value) {
        this.terminationTime = value;
    }

    /**
     * Gets the value of the currentTime property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentTime }
     *     
     */
    public CurrentTime getCurrentTime() {
        return currentTime;
    }

    /**
     * Sets the value of the currentTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentTime }
     *     
     */
    public void setCurrentTime(CurrentTime value) {
        this.currentTime = value;
    }

}
