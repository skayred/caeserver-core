
package org.caebeans.caeserver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="NOT_STARTED" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="RUNNING" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="HELD" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="SUCCESSFULL" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="FAILED" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "notstarted",
    "running",
    "held",
    "successfull",
    "failed"
})
@XmlRootElement(name = "Status")
public class Status {

    @XmlElement(name = "NOT_STARTED")
    protected Object notstarted;
    @XmlElement(name = "RUNNING")
    protected Object running;
    @XmlElement(name = "HELD")
    protected Object held;
    @XmlElement(name = "SUCCESSFULL")
    protected Object successfull;
    @XmlElement(name = "FAILED")
    protected Object failed;

    /**
     * Gets the value of the notstarted property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getNOTSTARTED() {
        return notstarted;
    }

    /**
     * Sets the value of the notstarted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setNOTSTARTED(Object value) {
        this.notstarted = value;
    }

    /**
     * Gets the value of the running property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getRUNNING() {
        return running;
    }

    /**
     * Sets the value of the running property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setRUNNING(Object value) {
        this.running = value;
    }

    /**
     * Gets the value of the held property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getHELD() {
        return held;
    }

    /**
     * Sets the value of the held property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setHELD(Object value) {
        this.held = value;
    }

    /**
     * Gets the value of the successfull property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getSUCCESSFULL() {
        return successfull;
    }

    /**
     * Sets the value of the successfull property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setSUCCESSFULL(Object value) {
        this.successfull = value;
    }

    /**
     * Gets the value of the failed property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getFAILED() {
        return failed;
    }

    /**
     * Sets the value of the failed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setFAILED(Object value) {
        this.failed = value;
    }

}
