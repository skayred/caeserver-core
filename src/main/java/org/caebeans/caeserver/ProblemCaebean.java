
package org.caebeans.caeserver;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "categories",
        "resources",
        "licenses"
})
@XmlRootElement(name = "problemCaebean")
public class ProblemCaebean {

    @XmlElement(required = true)
    protected ProblemCaebean.Categories categories;
    @XmlElement(required = true)
    protected ProblemCaebean.Resources resources;
    @XmlElement(required = true)
    protected ProblemCaebean.Licenses licenses;
    @XmlAttribute
    protected String name;
    @XmlAttribute
    protected String author;
    @XmlAttribute
    protected BigDecimal version;
    @XmlAttribute
    protected String caebeanId;

    public ProblemCaebean.Categories getCategories() {
        return categories;
    }

    public void setCategories(ProblemCaebean.Categories value) {
        this.categories = value;
    }

    public ProblemCaebean.Resources getResources() {
        return resources;
    }

    public void setResources(ProblemCaebean.Resources value) {
        this.resources = value;
    }

    public ProblemCaebean.Licenses getLicenses() {
        return licenses;
    }

    public void setLicenses(ProblemCaebean.Licenses licenses) {
        this.licenses = licenses;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String value) {
        this.author = value;
    }

    public BigDecimal getVersion() {
        return version;
    }

    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    public String getCaebeanId() {
        return caebeanId;
    }

    public void setCaebeanId(String value) {
        this.caebeanId = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "category"
    })
    public static class Categories {

        @XmlElement(required = true)
        protected ProblemCaebean.Categories.Category category;

        public ProblemCaebean.Categories.Category getCategory() {
            return category;
        }

        public void setCategory(ProblemCaebean.Categories.Category value) {
            this.category = value;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "parameter"
        })
        public static class Category {

            @XmlElement(required = true)
            protected List<Parameter> parameter;
            @XmlAttribute
            protected String name;
            @XmlAttribute
            protected String data;

            public List<Parameter> getParameter() {
                if (parameter == null) {
                    parameter = new ArrayList<Parameter>();
                }
                return this.parameter;
            }

            public String getName() {
                return name;
            }

            public void setName(String value) {
                this.name = value;
            }

            public String getData() {
                return data;
            }

            public void setData(String value) {
                this.data = value;
            }

        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "resource"
    })
    public static class Resources {

        @XmlElement(required = true)
        protected List<Resource> resource;

        public List<Resource> getResources() {
            if (resource == null) {
                resource = new ArrayList<Resource>();
            }
            return this.resource;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "license"
    })
    public static class Licenses {

        @XmlElement(required = true)
        protected List<License> license;

        public List<License> getLicenses() {
            if (license == null) {
                license = new ArrayList<License>();
            }
            return this.license;
        }
    }

}
