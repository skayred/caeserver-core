package org.caebeans.task.builder;

import com.google.inject.Inject;
import org.caebeans.lib.LoggerManager;
import org.caebeans.lib.MapHelpers;
import org.caebeans.storage.ProjectStorageManager;
import org.caebeans.storage.WorkStorageManager;
import org.caebeans.task.description.Context;
import org.caebeans.task.description.TaskDescription;
import org.caebeans.task.description.nodes.BaseNode;
import org.caebeans.task.description.nodes.InitialNode;
import org.caebeans.task.description.nodes.MergeNode;
import org.caebeans.task.description.nodes.NextNodePair;
import org.caebeans.xmlbeans.scheduler.awf.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/22/12
 * Time: 9:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class TaskBuilderImpl implements TaskBuilder {
    public static final String NONE_EXEC = "none";
    public static final String NONE_EXPRESSION = "none";
    private String projectId;
    private TaskDescription description;
    private Document project;

    @Inject
    private LoggerManager logger;
    @Inject
    private ProjectStorageManager storageManager;
    @Inject
    private WorkStorageManager workStorageManager;

    @Inject
    TaskBuilderImpl() {
    }

    @Override
    public TaskDescription build(String projectId) throws IOException, SAXException, ParserConfigurationException {
        this.projectId = projectId;

        logger.info("Building task description for project " + projectId);

        description = new TaskDescription();
        description.setProjectId(projectId);

        logger.info("Documents initialization");
        initDocument();

        logger.info("Project description parsing");
        parseProject();

        return description;
    }

    private void parseProject() {
        //  Component CAEBean parsing

        description.setName(project.getDocumentElement().getAttribute("name"));

        Element conceptualLayer = (Element) project.getElementsByTagName("conceptual").item(0);
        NodeList problemCAEBeans = conceptualLayer.getElementsByTagName("caebean");

        for (int i = 0 ; i < problemCAEBeans.getLength() ; i++) {
            Element problemNode = (Element) problemCAEBeans.item(i);

            String problemCAEBeanFilename = problemNode.getAttribute("fileName").replace(".xml", "");

            description.addProblemCAEBean(problemCAEBeanFilename);
        }

        Element physicalLayer = (Element) project.getElementsByTagName("physical").item(0);
        Map<String, Context> contexts = new HashMap<String, Context>();

        NodeList components = physicalLayer.getElementsByTagName("componentCaebean");
        for (int i = 0; i < components.getLength(); i++) {
            Node componentNode = components.item(i);

            if (componentNode.getNodeType() == Node.ELEMENT_NODE) {
                Element component = (Element) componentNode;
                Context context = new Context();

                NodeList paramTags = component.getElementsByTagName("param");
                NodeList inputTags = component.getElementsByTagName("input");
                NodeList outputTags = component.getElementsByTagName("output");

                context.setInputs(readFilenames(inputTags));
                context.setParams(MapHelpers.merge(readParams(inputTags), readAdditionalParams(paramTags)));

                List<String> outputs = readFilenames(outputTags);
                context.setOutputs(outputs);
                description.addOutputs(outputs);

                Element exec = (Element) component.getElementsByTagName("exec").item(0);
                context.setExecName(exec.getAttribute("name"));
                context.setExecAppName(exec.getAttribute("appname"));
                context.setAppVersion(exec.getAttribute("appversion"));

                NodeList procs = exec.getElementsByTagName("proc");

                for (int j = 0; j < procs.getLength(); j++) {
                    Node procNode = procs.item(j);

                    if (procNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element node = (Element) procNode;

                        String scriptName = node.getAttribute("name");
                        context.addProc(scriptName, node.getAttribute("output_name"));
                        context.addInput(scriptName);
                    }
                }

                contexts.put(component.getAttribute("name"), context);

                logger.debug(context);
            }
        }

        //*********************************//
        //  Workflow CAEBeans parsing

        Element logicalLayer = (Element) project.getElementsByTagName("logical").item(0);
        InitialNode initialNode = null;

        //  Node prasing
        NodeList nodes = logicalLayer.getElementsByTagName("node");
        Map<String, BaseNode> unorderedNodes = new HashMap<String, BaseNode>();
        Map<String, NodeDocument.Node> unorderedBrokerNodes = new HashMap<String, NodeDocument.Node>();

        AbstractWorkflowDocument.AbstractWorkflow awf = AbstractWorkflowDocument.AbstractWorkflow.Factory.newInstance();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element nodeElement = (Element) node;
                String id = nodeElement.getAttribute("id");

                BaseNode createdNode = BaseNode.createNode(id, nodeElement.getAttribute("type"));
                NodeDocument.Node brokerNode = BaseNode.createBrokerNode(id, nodeElement.getAttribute("type"), awf);

                String exec = nodeElement.getAttribute("execute");
                String expression = nodeElement.getAttribute("expression");

                System.out.println("EXPRESSION: " + expression);

                if (!exec.equalsIgnoreCase(NONE_EXEC)) {
                    createdNode.setContext(contexts.get(exec));
                }

                if (createdNode.getType() == BaseNode.ACTIVE_NODE) {
                    Context context = contexts.get(exec);

                    TACTIVITY activity = brokerNode.getType().getACTIVITY();
                    TApplication application = activity.addNewApplication();
                    application.setName(context.getExecAppName());
                    application.setVersion(context.getAppVersion());
                }

                if (!expression.equalsIgnoreCase("") && !expression.equalsIgnoreCase(NONE_EXPRESSION)) {
                    createdNode.setExpression(expression);
                }

                if (createdNode.getType() == BaseNode.INITIAL_NODE) {
                    initialNode = (InitialNode) createdNode;
                }

                unorderedNodes.put(id, createdNode);
                unorderedBrokerNodes.put(id, brokerNode);

                logger.debug(createdNode);
            }
        }

        //  Edge parsing
        NodeList edges = logicalLayer.getElementsByTagName("edge");

        for (int i = 0; i < edges.getLength(); i++) {
            Node edgeNode = edges.item(i);

            if (edgeNode.getNodeType() == Node.ELEMENT_NODE) {
                Element edge = (Element) edgeNode;

                EdgeDocument.Edge brokerEdge = awf.addNewEdge();
                brokerEdge.setId(new Integer(i).toString());
                brokerEdge.setFromNode(edge.getAttribute("source"));
                brokerEdge.setToNode(edge.getAttribute("target"));

                BaseNode source = unorderedNodes.get(edge.getAttribute("source"));
                BaseNode target = unorderedNodes.get(edge.getAttribute("target"));
                String condition = edge.getAttribute("condition");

                if (target instanceof MergeNode) {
                    ((MergeNode) target).incrementInputCount();
                }

                source.next.add(new NextNodePair(target, condition));
            }
        }

        description.setInitialNode(initialNode);
        description.setAbstractWorkflow(awf);

//        System.out.println(description);
    }

    private void initDocument() throws ParserConfigurationException, SAXException, IOException {
        File projectXml = storageManager.getProjectDescription(projectId);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        project = dBuilder.parse(projectXml);
        project.getDocumentElement().normalize();
    }

    private List<String> readFilenames(NodeList tags) {
        List<String> filenames = new ArrayList<String>();

        for (int j = 0; j < tags.getLength(); j++) {
            Node inputNode = tags.item(j);

            if (inputNode.getNodeType() == Node.ELEMENT_NODE) {
                Element input = (Element) inputNode;

                filenames.add(input.getAttribute("filename"));
            }
        }

        return filenames;
    }

    private Map<String, String> readParams(NodeList tags) {
        Map<String, String> params = new HashMap<String, String>();

        for (int j = 0; j < tags.getLength(); j++) {
            Node inputNode = tags.item(j);

            if (inputNode.getNodeType() == Node.ELEMENT_NODE) {
                Element input = (Element) inputNode;

                String filename = input.getAttribute("filename");

                if (input.hasAttribute("param")) {
                    params.put(input.getAttribute("param"), filename);
                }
            }
        }

        return params;
    }

    private Map<String, String> readAdditionalParams(NodeList tags) {
        Map<String, String> params = new HashMap<String, String>();

        for (int j = 0; j < tags.getLength(); j++) {
            Node inputNode = tags.item(j);

            if (inputNode.getNodeType() == Node.ELEMENT_NODE) {
                Element input = (Element) inputNode;

                params.put(input.getAttribute("name"), input.getAttribute("value"));
            }
        }

        return params;
    }
}
