package org.caebeans.task.builder;

import org.caebeans.task.description.TaskDescription;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/26/12
 * Time: 2:14 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TaskBuilder {
    TaskDescription build(String projectId) throws IOException, SAXException, ParserConfigurationException;
}
