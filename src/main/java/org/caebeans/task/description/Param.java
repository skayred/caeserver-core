package org.caebeans.task.description;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/22/12
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class Param {
    public static final int FLOAT = 0;
    public static final int INTEGER = 1;
    public static final int STRING = 2;

    public static final Map<String, Integer> TYPES = Collections.unmodifiableMap(new HashMap<String, Integer>() {{
        put("Float", FLOAT);
        put("Integer", INTEGER);
        put("String", STRING);
    }});

    private int type;
    private Object value;

    public Param(int type, String value) {
        this.type = type;

        switch (type) {
            case FLOAT:
                this.value = Float.parseFloat(value);
                break;
            case INTEGER:
                this.value = Integer.parseInt(value);
                break;
            case STRING:
                this.value = value;
                break;
        }
    }

    public Object getValue() {
        return value;
    }

    public <T> T getValue(Class<T> klass) {
        return (T) value;
    }

    public int getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Params, type=" + type + ", value=" + value;
    }
}
