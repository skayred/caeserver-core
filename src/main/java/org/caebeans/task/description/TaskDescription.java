package org.caebeans.task.description;

import org.caebeans.task.description.nodes.BaseNode;
import org.caebeans.task.description.nodes.InitialNode;
import org.caebeans.task.description.nodes.NextNodePair;
import org.caebeans.xmlbeans.scheduler.awf.AbstractWorkflowDocument;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/22/12
 * Time: 9:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class TaskDescription implements Serializable {
    private Set<String> problemCAEBeans;
    private InitialNode initialNode;
    private String projectId;
    private String name;
    private String instanceId;
    private AbstractWorkflowDocument.AbstractWorkflow abstractWorkflow;
    public Set<String> outputs;

    public TaskDescription() {
        outputs = new HashSet<String>();
        problemCAEBeans = new HashSet<String>();
    }

    public void setInitialNode(InitialNode initialNode) {
        this.initialNode = initialNode;
    }

    public InitialNode getInitialNode() {
        return initialNode;
    }

    @Override
    public String toString() {
        return "{taskNodes : \r\n" + initialNode + "}";
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void addProblemCAEBean(String problemCAEBean) {
        problemCAEBeans.add(problemCAEBean);
    }

    public void addProblemCAEBeans(List<String> problems) {
        problemCAEBeans.addAll(problems);
    }

    public void addOutputs(List<String> outputs) {
        outputs.addAll(outputs);
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;

        setInstanceNodesInstanceId(initialNode, instanceId);
    }

    public void setAbstractWorkflow(AbstractWorkflowDocument.AbstractWorkflow awf) {
        abstractWorkflow = awf;
    }

    public AbstractWorkflowDocument.AbstractWorkflow asAbstractWorkflow() {
        return abstractWorkflow;
    }

    private void setInstanceNodesInstanceId(BaseNode node, String instanceId) {
        if ((node.getInstanceId() != null) && (!node.getInstanceId().equals(""))) { return; }

        node.setInstanceId(instanceId);

        for (NextNodePair nextNode : node.next) {
            setInstanceNodesInstanceId(nextNode.nextNode, instanceId);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addOutput(String output) {
        this.outputs.add(output);
    }

    public Set<String> getProblemCAEBeans() {
        return problemCAEBeans;
    }
}
