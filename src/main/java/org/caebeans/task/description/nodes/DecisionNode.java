package org.caebeans.task.description.nodes;

import bsh.EvalError;
import bsh.Interpreter;
import org.apache.log4j.Logger;
import org.caebeans.lib.JmteEval;
import org.caebeans.lib.Param;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.util.Iterator;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/22/12
 * Time: 9:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class DecisionNode extends BaseNode {
    public DecisionNode(String id) {
        super(id);
    }

    @Override
    public int getType() {
        return DECISION_NODE;
    }

    @Override
    public float exec(JmteEval phpEval,
                      Logger instanceLogger,
                      Map<String, Param> paramsHash, EndpointReferenceType endpointReferenceType) throws EvalError {
        Interpreter interpreter = new Interpreter();

        StringBuilder evalBuilder = new StringBuilder();
        Iterator iterator = paramsHash.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Param> pair = (Map.Entry<String, Param>) iterator.next();

            interpreter.set(pair.getKey(), pair.getValue().getValue());

            if (!getExpression().contains(pair.getKey())) {
                evalBuilder.append(";");
                evalBuilder.append(pair.getKey());
                evalBuilder.append("=");
                if (pair.getValue().getType() == Param.STRING) {
                    evalBuilder.append("\"");
                    evalBuilder.append(pair.getValue().getValue());
                    evalBuilder.append("\"");
                } else {
                    evalBuilder.append(pair.getValue().getValue());
                }
            }
        }

        interpreter.eval("result=" + getExpression() + evalBuilder.toString());

        iterator = paramsHash.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Param> pair = (Map.Entry<String, Param>) iterator.next();

            int type = (pair.getValue().getType() == Param.STRING) ? Param.STRING : Param.FLOAT;
            paramsHash.put(pair.getKey(), new Param(type, interpreter.get(pair.getKey()).toString()));
        }

        try {
            return (Float) interpreter.get("result");
        } catch (ClassCastException e) {
            instanceLogger.info("Expecting float, got int", e);

            return ((Integer) interpreter.get("result")).floatValue();
        }
    }

    public boolean eval(String condition, Map<String, Param> paramsHash) throws EvalError {
        Interpreter interpreter = new Interpreter();

        Iterator iterator = paramsHash.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Param> pair = (Map.Entry<String, Param>) iterator.next();

            interpreter.set(pair.getKey(), pair.getValue().getValue());
        }

        System.out.println("result=" + condition);

        interpreter.eval("result=" + condition);

        return (Boolean) interpreter.get("result");
    }
}
