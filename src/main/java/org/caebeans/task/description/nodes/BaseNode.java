package org.caebeans.task.description.nodes;

import org.apache.log4j.Logger;
import org.caebeans.lib.JmteEval;
import org.caebeans.lib.Param;
import org.caebeans.task.description.Context;
import org.caebeans.xmlbeans.scheduler.awf.AbstractWorkflowDocument;
import org.caebeans.xmlbeans.scheduler.awf.NodeDocument;
import org.caebeans.xmlbeans.scheduler.awf.TNode;
import org.codehaus.jackson.annotate.JsonProperty;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/22/12
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseNode implements Serializable {
    public static final int INITIAL_NODE = 0;
    public static final int DECISION_NODE = 1;
    public static final int FORK_NODE = 2;
    public static final int ACTIVE_NODE = 3;
    public static final int JOIN_NODE = 4;
    public static final int MERGE_NODE = 5;
    public static final int FINAL_NODE = 6;

    public static final Map<String, Integer> TYPES = Collections.unmodifiableMap(new HashMap<String, Integer>() {{
        put("begin", INITIAL_NODE);
        put("initial", INITIAL_NODE);
        put("decision", DECISION_NODE);
        put("fork", FORK_NODE);
        put("active", ACTIVE_NODE);
        put("join", JOIN_NODE);
        put("merge", MERGE_NODE);
        put("final", FINAL_NODE);
        put("end", FINAL_NODE);
    }});

    public ArrayList<NextNodePair> next;

    @JsonProperty
    protected Context context;
    private String id;
    private int x;
    private int y;
    private String expression;

    public BaseNode(String id) {
        this.next = new ArrayList<NextNodePair>();
        this.id = id;
    }

    @Override
    public String toString() {
        return "BaseNode";
//        return "\r\n\t{\r\n\ttype : " + getReadableType() + ", \r\n\tcontext : " + context + ", \r\n\tnext : " + next.toString() + "}";
    }

    public abstract float exec(JmteEval phpEval,
                               Logger instanceLogger,
                               Map<String, Param> paramsHash, EndpointReferenceType endpointReferenceType) throws Exception;

    public String getReadableType() {
        switch (this.getType()) {
            case INITIAL_NODE:
                return "Initial node";
            case DECISION_NODE:
                return "Desicion node";
            case FORK_NODE:
                return "Fork node";
            case ACTIVE_NODE:
                return "Active node";
            case JOIN_NODE:
                return "Join node";
            case MERGE_NODE:
                return "Merge node";
            case FINAL_NODE:
                return "Final node";
        }

        return "";
    }

    public abstract int getType();

    public void setContext(Context context) {
        this.context = context;
    }

    public void setInstanceId(String instanceId) {
        if (context != null) {
            this.context.setInstanceId(instanceId);
        }
    }

    public String getInstanceId() {
        if (context != null) {
            return this.context.getInstanceId();
        }

        return null;
    }

    public String getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        System.out.println("Setting expression " + expression + " for node " + getId());

        this.expression = expression;
    }

    public static NodeDocument.Node createBrokerNode(String id, String typeString, AbstractWorkflowDocument.AbstractWorkflow awf) {
        int type = TYPES.get(typeString);

        NodeDocument.Node brokerNode = awf.addNewNode();
        brokerNode.setId(id);
        TNode nodeType = TNode.Factory.newInstance();

        switch (type) {
            case INITIAL_NODE:
                nodeType.addNewBEGIN();
                break;
            case DECISION_NODE:
                nodeType.addNewBEGIN();
                break;
            case FORK_NODE:
                nodeType.addNewBEGIN();
                break;
            case ACTIVE_NODE:
                nodeType.addNewACTIVITY();
                break;
            case JOIN_NODE:
                nodeType.addNewEND();
                break;
            case MERGE_NODE:
                nodeType.addNewEND();
                break;
            case FINAL_NODE:
                nodeType.addNewEND();
                break;
        }
        brokerNode.setType(nodeType);

        return brokerNode;
    }

    public static BaseNode createNode(String id, String typeString) {
        int type = TYPES.get(typeString);
        BaseNode node = null;

        //  TODO: normal types
        switch (type) {
            case INITIAL_NODE:
                node = new InitialNode(id);
                break;
            case DECISION_NODE:
                node = new DecisionNode(id);
                break;
            case FORK_NODE:
                node = new ForkNode(id);
                break;
            case ACTIVE_NODE:
                node = new ActiveNode(id);
                break;
            case JOIN_NODE:
                node = new JoinNode(id);
                break;
            case MERGE_NODE:
                node = new MergeNode(id);
                break;
            case FINAL_NODE:
                node = new FinalNode(id);
                break;
        }

        return node;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BaseNode) {
            BaseNode other = (BaseNode) o;

            if (other.id.equalsIgnoreCase(this.id)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
