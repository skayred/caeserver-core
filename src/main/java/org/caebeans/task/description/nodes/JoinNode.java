package org.caebeans.task.description.nodes;

import org.apache.log4j.Logger;
import org.caebeans.lib.JmteEval;
import org.caebeans.lib.Param;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/22/12
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class JoinNode extends BaseNode {
    public JoinNode(String id) {
        super(id);
    }

    @Override
    public int getType() {
        return JOIN_NODE;
    }

    @Override
    public float exec(JmteEval phpEval,
                      Logger instanceLogger,
                      Map<String, Param> paramsHash, EndpointReferenceType endpointReferenceType) {
        System.out.println("Hello, I'm join node");

        return 0;
    }
}
