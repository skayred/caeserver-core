package org.caebeans.task.description.nodes;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.log4j.Logger;
import org.caebeans.ServerModule;
import org.caebeans.lib.JmteEval;
import org.caebeans.lib.Param;
import org.caebeans.storage.WorkStorageManager;
import org.caebeans.unicore.client.Client;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/22/12
 * Time: 9:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class ActiveNode extends BaseNode {
    public ActiveNode(String id) {
        super(id);
    }

    @Override
    public int getType() {
        return ACTIVE_NODE;
    }

    @Override
    public float exec(JmteEval phpEval,
                      Logger instanceLogger,
                      Map<String, Param> paramsHash,
                      EndpointReferenceType tss) throws Exception {
        Injector injector = Guice.createInjector(new ServerModule());
        WorkStorageManager workStorageManager = injector.getInstance(WorkStorageManager.class);
        Client client = injector.getInstance(Client.class);

        List<String> inputs = processFileList(context.getInputs(), paramsHash);
        List<String> outputs = processFileList(context.getOutputs(), paramsHash);
        Map<String, String> processedParams = processParamMap(context.getParams(), paramsHash);

        File nodeDir = workStorageManager.createNodeDirectory(context.getInstanceId(),
                getId(),
                inputs,
                instanceLogger);

        Map<String, String> procs = context.getProcs();

        for (Map.Entry<String, String> proc : procs.entrySet()) {
            phpEval.eval(new File(nodeDir.getAbsolutePath(), proc.getKey()),
                    new File(nodeDir.getAbsolutePath(), proc.getValue()));
        }

        client.run(nodeDir.getAbsolutePath(),
                context.getExecAppName(),
                context.getAppVersion(),
                processedParams,
                inputs,
                outputs,
                instanceLogger,
                tss);

        workStorageManager.deleteNodeDirectory(context.getInstanceId(),
                getId(),
                outputs,
                instanceLogger);

        return 0;
    }

    private List<String> processFileList(List<String> list, Map<String, Param> paramsHash) {
        List<String> inputs = new ArrayList<String>();

        for (int i = 0 ; i < list.size() ; i++) {
            String parameter = list.get(i);

            System.out.println("Param " + parameter);

            if (parameter.contains("$")) {
                StringBuilder newParameterText = new StringBuilder();
                String[] segments = parameter.split("\\$");

                for (int segmentNumber = 0 ; segmentNumber < segments.length ; segmentNumber++) {
                    if (segmentNumber % 2 == 1) {
                        newParameterText.append(paramsHash.get(segments[segmentNumber]).getValue());
                    } else {
                        newParameterText.append(segments[segmentNumber]);
                    }
                }

                parameter = newParameterText.toString();
            }

            inputs.add(parameter);
        }

        return inputs;
    }

    private Map<String, String> processParamMap(Map<String, String> params, Map<String, Param> paramsHash) {
        Map<String, String> newParams = new HashMap<String, String>();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            String parameter = entry.getValue();

            if (parameter.contains("$")) {
                StringBuilder newParameterText = new StringBuilder();
                String[] segments = parameter.split("\\$");

                for (int segmentNumber = 0 ; segmentNumber < segments.length ; segmentNumber++) {

                    System.out.println("Parameter name " + parameter);

                    if (segmentNumber % 2 == 1) {
                        newParameterText.append(paramsHash.get(segments[segmentNumber]).getValue());
                    } else {
                        newParameterText.append(segments[segmentNumber]);
                    }
                }

                parameter = newParameterText.toString();
            }

            newParams.put(entry.getKey(), parameter);
        }

        return newParams;
    }
}
