package org.caebeans.task.description.nodes;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: dmitry
 * Date: 9/9/12
 * Time: 3:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class NextNodePair implements Serializable {
    public String condition;
    public BaseNode nextNode;

    public NextNodePair(BaseNode nextNode, String condition) {
        this.nextNode = nextNode;
        this.condition = condition;
    }

    @Override
    public String toString() {
        return "{condition: " + condition + ", nextNode: " + nextNode + "}";
    }
}
