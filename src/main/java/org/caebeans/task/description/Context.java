package org.caebeans.task.description;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/22/12
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class Context implements Serializable {
    private List<String> inputs;
    private List<String> outputs;

    private String instanceId;

    private String paramName;
    private String paramValue;
    private String execName;
    private String execAppName;
    private String appVersion;
    private Map<String, String> params;
    private Map<String, String> procs;

    public Context() {
        inputs = new ArrayList<String>();
        outputs = new ArrayList<String>();

        procs = new HashMap<String, String>();
    }

    @Override
    public String toString() {
        return "\t\t{\r\n\t\tinputs : " + inputs + ", \r\n\t\toutputs : " + outputs + ", \r\n\t\texecName : " + appVersion + ",\r\n\t\texecName : " + execAppName + ", \r\n\t\tprocs : " + procs + "}\r\n";
    }

    public String getExecAppName() {
        return execAppName;
    }

    public void setExecAppName(String execAppName) {
        this.execAppName = execAppName;
    }

    public String getExecName() {
        return execName;
    }

    public void setExecName(String execName) {
        this.execName = execName;
    }

    public List<String> getInputs() {
        return inputs;
    }

    public void setInputs(List<String> inputs) {
        this.inputs = inputs;
    }

    public List<String> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<String> outputs) {
        this.outputs = outputs;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public void addProc(String name, String outputName) {
        procs.put(name, outputName);
    }

    public Map<String, String> getProcs() {
        return procs;
    }

    public void addInput(String scriptName) {
        inputs.add(scriptName);
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}
