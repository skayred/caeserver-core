package org.caebeans;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.caebeans.admin.AdminServerInitializer;
import org.caebeans.admin.servlets.JadeHandler;
import org.caebeans.database.WorkManager;
import org.caebeans.lib.Config;
import org.caebeans.lib.Instances;
import org.caebeans.lib.LoggerManager;
import org.caebeans.lib.Serializer;
import org.caebeans.task.builder.TaskBuilder;
import org.caebeans.task.description.TaskDescription;
import org.caebeans.wsrf.CAEServerImpl;
import org.caebeans.wsrf.InstanceImpl;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;

import javax.xml.ws.Endpoint;
import javax.xml.ws.soap.SOAPBinding;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 4/22/12
 * Time: 9:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class CAEServerMain {
    public static final String SERVER_HOST = "caeserver.host";
    public static final String SERVER_PORT = "caeserver.port";
    public static final String SERVER_PATH = "caeserver";
    public static final String INSTANCE_PATH = "caeinstance";

    public static void main(String[] args) throws Exception {
        Injector injector = Guice.createInjector(new ServerModule());
        Instances instances = injector.getInstance(Instances.class);
        Config config = injector.getInstance(Config.class);
        LoggerManager logger = injector.getInstance(LoggerManager.class);
        WorkManager workManager = injector.getInstance(WorkManager.class);

        String serverHost = getServerHost(config);

        InstanceImpl instance = new InstanceImpl(instances);

        Endpoint serverEndpoint = Endpoint.publish(serverHost + SERVER_PATH, new CAEServerImpl(instances));
        Endpoint instanceEndpoint = Endpoint.publish(serverHost + INSTANCE_PATH, instance);

//        instance.checkForUnfinished();

        try {
            AdminServerInitializer.initAdminServer(config);
        } catch (IOException e) {
            logger.error("Failed to start admin server", e);
        }
    }

    private static String getServerHost(Config config) {
        String configHost = config.getConfig().getString(SERVER_HOST);
        String configPort = config.getConfig().getString(SERVER_PORT);

        String host = configHost + ":" + configPort + "/";

        return host;
    }
}
