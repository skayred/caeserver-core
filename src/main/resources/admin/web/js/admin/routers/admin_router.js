Admin.routers.AdminRouter = Backbone.Router.extend({
    routes:{
        "":"projects",
        "projects":"projects",
        "logs":"logs"
    },

    initialize:function (options) {
        var self = this;

        self.mainContainer = $("#main");
    },

    projects:function () {
        var view = new Admin.views.projects.List().render();
        $("#side-pane").html(view.el);
    },

    logs:function () {
        var view = new Admin.views.logs.List().render();
        $("#side-pane").html(view.el);
    }
});