Admin.models.Project = Backbone.Model.extend({
    defaults:{
    },

    url:function () {
        return "/api/projects/" + this.id;
    }
});