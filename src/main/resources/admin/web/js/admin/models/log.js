Admin.models.Log = Backbone.Model.extend({
    defaults:{
    },

    idAttribute: "filename",

    url:function () {
        return "/api/logs/" + this.id;
    }
});