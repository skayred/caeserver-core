Admin.collections.Projects = Backbone.Collection.extend({
    url: function() {
        return "/api/projects";
    },

    model: Admin.models.Project
});