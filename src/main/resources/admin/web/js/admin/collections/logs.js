Admin.collections.Logs = Backbone.Collection.extend({
    url: function() {
        return "/api/logs";
    },

    model: Admin.models.Log
});