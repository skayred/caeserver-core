Admin.views.logs.List = Backbone.View.extend({
    events:{
        "click .log": "showLog"
    },

    initialize:function (options) {
        var self = this;

        _.bindAll(this
            , "render"
            , "onLogsLoaded"
            , "showLog"
        );

        this.collection = new Admin.collections.Logs();
        this.collection.fetch();

        this.collection.bind("reset", this.onLogsLoaded);
    },

    render:function () {
        $(this.el).html($(JST["admin/logs"]()));

        this.logsContainer = this.$("#logs");

        return this;
    },

    onLogsLoaded:function () {
        var self = this;

        this.logsContainer.html('');

        this.collection.each(function (project) {
            var view = $(JST["admin/log_item"](project.toJSON()));

            self.logsContainer.append(view);
        });


        return false;
    },

    showLog: function(e) {
        var view = new Admin.views.logs.Log({model: this.collection.get(e.currentTarget.id)}).render();

        $("#main-pane").html(view.el);
    }
});