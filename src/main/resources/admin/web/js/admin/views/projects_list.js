Admin.views.projects.List = Backbone.View.extend({
    events:{
        "click .project": "showProject"
    },

    initialize:function (options) {
        var self = this;

        _.bindAll(this
            , "render"
            , "onProjectsLoaded"
            , "showProject"
        );

        this.collection = new Admin.collections.Projects();
        this.collection.fetch();

        this.collection.bind("reset", this.onProjectsLoaded);
    },

    render:function () {
        $(this.el).html($(JST["admin/projects"]()));

        this.logsContainer = this.$("#projects");

        return this;
    },

    onProjectsLoaded:function () {
        var self = this;

        this.logsContainer.html('');

        this.collection.each(function (project) {
            var view = $(JST["admin/project_item"](project.toJSON()));

            self.logsContainer.append(view);
        });


        return false;
    },

    showProject: function(e) {
        var view = new Admin.views.projects.Project({model: this.collection.get(e.currentTarget.id)}).render();

        $("#main-pane").html(view.el);
    }
});