Admin.views.logs.Log = Backbone.View.extend({
    events:{
    },

    initialize:function () {
        var self = this;

        _.bindAll(this
            , "render"
        );
    },

    render:function () {
        $(this.el).html($(JST["admin/log_details"](this.model.toJSON())));

        return this;
    }
});