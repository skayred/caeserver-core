Admin.views.projects.Project = Backbone.View.extend({
    events:{
        "click #project-nodes .active":"showDetails"
    },

    legendNodes: [
        {
            id:"legendText",
            x:0,
            y:0,
            type:"legend",
            text: "Nodes"
        },
        {
            id:"legendInitial",
            x:10,
            y:20,
            type:"initial"
        },
        {
            id:"legendText",
            x:0,
            y:70,
            type:"legend",
            text: "Initial"
        },
        {
            id:"legendFinal",
            x:10,
            y:90,
            type:"final"
        },
        {
            id:"legendText",
            x:0,
            y:140,
            type:"legend",
            text: "Final"
        },
        {
            id:"legend",
            x:10,
            y:160,
            type:"active"
        },
        {
            id:"legendText",
            x:0,
            y:210,
            type:"legend",
            text: "Active"
        },
        {
            id:"legendFork",
            x:10,
            y:230,
            type:"fork"
        },
        {
            id:"legendText",
            x:0,
            y:280,
            type:"legend",
            text: "Fork"
        },
        {
            id:"legendMerge",
            x:10,
            y:300,
            type:"merge"
        },
        {
            id:"legendText",
            x:0,
            y:350,
            type:"legend",
            text: "Merge"
        }
    ],

    initialize:function () {
        var self = this;

        _.bindAll(this
            , "render"
            , "prepareGraph"
            , "buildNodes"
            , "showDetails"
        );
    },

    render:function () {
        var self = this;

        $(this.el).html($(JST["admin/project_details"](this.model.toJSON())));

        this.prepareGraph();

        return this;
    },

    prepareGraph:function () {
        var self = this;

        var width = $("#main-pane").width() - 70;
        var height = $("#main-pane").height();

        var foci = [
            {x:-50, y:height / 2},
            //  For initial node
            {x:width / 2, y:height / 2},
            //  For all the rest
            {x:width + 50, y:height / 2}   //   For final node
        ];
        var centroids = {
            "initial":foci[0],
            "decision":foci[1],
            "fork":foci[1],
            "active":foci[1],
            "join":foci[1],
            "merge":foci[1],
            "final":foci[2]
        };

        var containerWidth = 50;
        var containerHeight = 50;

        var color = d3.scale.category20();

        var force = d3.layout.force()
            .charge(-1000)
            .linkDistance(150)
            .size([width, height]);

        var svg = d3.select(this.$("#chart").get(0))
            .append("svg")
            .attr("id", "project-nodes");
//            .attr("width", width)
//            .attr("height", height);

        var json = this.buildNodes();

        force
            .nodes(json.nodes)
            .links(json.links)
            .gravity(0)
            .start();

        force.on("tick", function (e) {
            var k = .1 * e.alpha;
            json.nodes.forEach(function (o, i) {
                o.y += (centroids[o.type].y - o.y) * k;
                o.x += (centroids[o.type].x - o.x) * k;
            });

            svg.selectAll(".node")
                .attr("x", function (d) {
                    return d.x;
                })
                .attr("y", function (d) {
                    return d.y;
                });

            link.attr("x1", function (d) {
                return d.source.x + containerWidth / 2;
            })
                .attr("y1", function (d) {
                    return d.source.y + containerHeight / 2;
                })
                .attr("x2", function (d) {
                    return d.target.x + containerWidth / 2;
                })
                .attr("y2", function (d) {
                    return d.target.y + containerHeight / 2;
                });

            node
                .attr("x", function (d) {
                    return d.x;
                })
                .attr("y", function (d) {
                    return d.y;
                });
        });

        var link = svg.selectAll("line.link")
            .data(json.links)
            .enter().append("line")
            .attr("class", "link")
            .style("stroke-width", function (d) {
                return Math.sqrt(d.value);
            });

        var path = svg.append("svg:g").selectAll("path")
            .data(force.links())
            .enter().append("svg:path")
            .attr("class", function (d) {
                return "link " + d.type;
            })
            .attr("marker-end", function (d) {
                return "url(#" + d.type + ")";
            });

        var node = svg.selectAll(".node")
            .data(json.nodes)
            .enter()
            .append("svg")
            .attr("width", containerWidth)
            .attr("height", containerHeight)
            .attr("x", function (d) {
                return d.x;
            })
            .attr("y", function (d) {
                return d.y;
            })
            .attr("class", function (d) {
                return d.type + " node";
            })
            .attr("id", function (d) {
                return d.id;
            })
            .style("fill", function (d) {
                return color(d.group);
            })
            .call(force.drag);

        this.renderNodes(svg);

        var legendSvg = d3.select(this.$("#legend").get(0))
            .append("svg")
            .attr("id", "project-legend");

        var legendNode = legendSvg.selectAll(".node")
            .data(this.legendNodes)
            .enter()
            .append("svg")
            .attr("width", 70)
            .attr("height", function(d) {
                if (d.type === "legend") {
                    return 20;
                } else {
                    return 50;
                }
            })
            .attr("x", function (d) {
                return d.x;
            })
            .attr("y", function (d) {
                return d.y;
            })
            .attr("class", function (d) {
                return d.type + " node";
            })
            .attr("id", function (d) {
                return d.id;
            })
            .style("fill", function (d) {
                return color(d.group);
            });

        this.renderNodes(legendSvg);
    },

    buildNodes:function () {
        var types = {
            0:"initial",
            1:"decision",
            2:"fork",
            3:"active",
            4:"join",
            5:"merge",
            6:"final"
        };

        var self = this;
        this.contexts = new Object();
        var nodes = new Object();
        var nodeList = new Array();
        var links = new Array();

        function getNode(oldNode) {
            return {
                id:oldNode.id,
                x:oldNode.x,
                y:oldNode.y,
                type:types[oldNode.type]
            };
        }

        function addNode(number, node) {
            for (var i = 0; i < node.next.length; i++) {
                var currentNode = node.next[i];

                if (nodes[currentNode.id] == null) {
                    var nextNumber = nodeList.length;

                    nodes[currentNode.id] = nextNumber;
                    self.contexts[currentNode.id] = currentNode.context;

                    nodeList.push(getNode(currentNode));
                    links.push({
                        source:number,
                        target:nextNumber
                    });

                    addNode(nextNumber, currentNode);
                } else {
                    links.push({
                        source:number,
                        target:nodes[currentNode.id]
                    });
                }
            }
        }

        nodeList.push(getNode(this.model.toJSON().initialNode));
        addNode(0, this.model.toJSON().initialNode);

        console.log(this.contexts);

        return {
            nodes:nodeList,
            links:links
        };
    },

    renderNodes:function (svg) {
        svg.selectAll(".legend")
            .append("line")
            .attr("x1", 0)
            .attr("y1", 18)
            .attr("x2", 70)
            .attr("y2", 18)
            .attr("class", "legend-icon");
        svg.selectAll(".legend")
            .append("text")
            .attr("x", 14)
            .attr("y", 12)
            .attr("font-family", "monospace")
            .text(function (d) {
                return d.text;
            });

        svg.selectAll(".initial")
            .append("circle")
            .attr("r", 12.5)
            .attr("cx", 25)
            .attr("cy", 25)
            .attr("class", "initial-icon");

        svg.selectAll(".final")
            .append("circle")
            .attr("r", 7.5)
            .attr("cx", 25)
            .attr("cy", 25)
            .attr("class", "final-icon");

        svg.selectAll(".active")
            .append("rect")
            .attr("y", 10)
            .attr("rx", 8)
            .attr("ry", 8)
            .attr("width", 50)
            .attr("height", 30)
            .attr("class", "active-icon");
        svg.selectAll(".active")
            .append("text")
            .attr("font-family", "monospace")
            .attr("font-size", "13px")
            .attr("x", 4)
            .attr("y", 30)
            .text(function (d) {
                return d.id;
            });

        svg.selectAll(".fork")
            .append("polygon")
            .attr("points", "25 12.5, 37.5 25, 25 37.5, 12.5 25")
            .attr("class", "fork-icon");
        svg.selectAll(".fork")
            .append("line")
            .attr("x1", 0)
            .attr("y1", 25)
            .attr("x2", 12.5)
            .attr("y2", 25)
            .attr("class", "fork-icon");
        svg.selectAll(".fork")
            .append("line")
            .attr("x1", 25)
            .attr("y1", 0)
            .attr("x2", 25)
            .attr("y2", 12.5)
            .attr("class", "fork-icon");
        svg.selectAll(".fork")
            .append("line")
            .attr("x1", 37.5)
            .attr("y1", 25)
            .attr("x2", 50)
            .attr("y2", 25)
            .attr("class", "fork-icon");

        svg.selectAll(".merge")
            .append("polygon")
            .attr("points", "25 12.5, 37.5 25, 25 37.5, 12.5 25")
            .attr("class", "fork-icon");
        svg.selectAll(".merge")
            .append("line")
            .attr("x1", 0)
            .attr("y1", 25)
            .attr("x2", 12.5)
            .attr("y2", 25)
            .attr("class", "fork-icon");
        svg.selectAll(".merge")
            .append("line")
            .attr("x1", 25)
            .attr("y1", 50)
            .attr("x2", 25)
            .attr("y2", 37.5)
            .attr("class", "fork-icon");
        svg.selectAll(".merge")
            .append("line")
            .attr("x1", 37.5)
            .attr("y1", 25)
            .attr("x2", 50)
            .attr("y2", 25)
            .attr("class", "fork-icon");
    },

    showDetails:function (e) {
        function buildNode(name) {
            return {
                name:name
            };
        }

        function buildTree(context) {
            var first = buildNode(context.execName);

            var inputs = buildNode("inputs");
            inputs.children = [];
            for (var i = 0; i < context.inputs.length; i++) {
                inputs.children.push(buildNode(context.inputs[i]));
            }

            var outputs = buildNode("outputs");
            outputs.children = [];
            for (var i = 0; i < context.outputs.length; i++) {
                outputs.children.push(buildNode(context.outputs[i]));
            }

            first.children = [];
            first.children.push(inputs);
            first.children.push(outputs);

            return first;
        }

        function update(source) {
            var duration = d3.event && d3.event.altKey ? 5000 : 500;

            // Compute the new tree layout.
            var nodes = tree.nodes(root).reverse();

            // Normalize for fixed-depth.
            nodes.forEach(function (d) {
//                d.y = d.depth * 180;
                d.y = d.depth * 90;
            });

            // Update the nodes…
            var node = vis.selectAll("g.details-node")
                .data(nodes, function (d) {
                    return d.id || (d.id = ++i);
                });

            // Enter any new nodes at the parent's previous position.
            var nodeEnter = node.enter().append("svg:g")
                .attr("class", "details-node")
                .attr("transform", function (d) {
                    return "translate(" + source.y0 + "," + source.x0 + ")";
                })
                .on("click", function (d) {
                    toggle(d);
                    update(d);
                });

            nodeEnter.append("svg:circle")
                .attr("r", 1e-6)
                .style("fill", function (d) {
                    return d._children ? "lightsteelblue" : "#fff";
                });

            nodeEnter.append("svg:text")
                .attr("x", function (d) {
                    return d.children || d._children ? -10 : 10;
                })
                .attr("dy", ".35em")
                .attr("text-anchor", function (d) {
                    return d.children || d._children ? "end" : "start";
                })
                .text(function (d) {
                    return d.name;
                })
                .style("fill-opacity", 1e-6);

            // Transition nodes to their new position.
            var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + d.y + "," + d.x + ")";
                });

            nodeUpdate.select("circle")
                .attr("r", 4.5)
                .style("fill", function (d) {
                    return d._children ? "lightsteelblue" : "#fff";
                });

            nodeUpdate.select("text")
                .style("fill-opacity", 1);

            // Transition exiting nodes to the parent's new position.
            var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + source.y + "," + source.x + ")";
                })
                .remove();

            nodeExit.select("circle")
                .attr("r", 1e-6);

            nodeExit.select("text")
                .style("fill-opacity", 1e-6);

            // Update the links…
            var link = vis.selectAll("path.details-link")
                .data(tree.links(nodes), function (d) {
                    return d.target.id;
                });

            // Enter any new links at the parent's previous position.
            link.enter().insert("svg:path", "g")
                .attr("class", "details-link")
                .attr("d", function (d) {
                    var o = {x:source.x0, y:source.y0};
                    return diagonal({source:o, target:o});
                })
                .transition()
                .duration(duration)
                .attr("d", diagonal);

            // Transition links to their new position.
            link.transition()
                .duration(duration)
                .attr("d", diagonal);

            // Transition exiting nodes to the parent's new position.
            link.exit().transition()
                .duration(duration)
                .attr("d", function (d) {
                    var o = {x:source.x, y:source.y};
                    return diagonal({source:o, target:o});
                })
                .remove();

            // Stash the old positions for transition.
            nodes.forEach(function (d) {
                d.x0 = d.x;
                d.y0 = d.y;
            });
        }

        function toggle(d) {
            if (d.children) {
                d._children = d.children;
                d.children = null;
            } else {
                d.children = d._children;
                d._children = null;
            }
        }

        var view = $(JST["admin/node_details"]());
        var treeNodes = buildTree(this.contexts[e.currentTarget.id]);

        ui.dialog(view)
            .closable()
            .overlay()
            .show();

        var m = [20, 40, 20, 40],
            w = 320 - m[1] - m[3],
            h = 320 - m[0] - m[2],
            i = 0,
            root;

        var tree = d3.layout.tree()
            .size([h, w]);

        var diagonal = d3.svg.diagonal()
            .projection(function (d) {
                return [d.y, d.x];
            });

        var vis = d3.select($("#node-chart").get(0)).append("svg:svg")
            .attr("width", w + m[1] + m[3])
//            .attr("width", w)
            .attr("height", h + m[0] + m[2])
//            .attr("height", h)
            .append("svg:g")
            .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

        root = treeNodes;
        root.x0 = h / 2;
        root.y0 = 0;

        update(root);
    }
});