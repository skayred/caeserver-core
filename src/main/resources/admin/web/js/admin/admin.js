Admin = {
    views:{
        projects: {},
        logs: {}
    },
    helpers:{},
    models:{},
    collections:{},
    routers:{},

    init:function () {
        this.routers.appRouter = new Admin.routers.AdminRouter();
        Backbone.history.start();
    }
};

Backbone.Model.prototype.idAttribute = "projectId";