#!/bin/bash

DIR=$(pwd)

echo "Creating config directory..."

cd $HOME
mkdir .caebeans

echo "Copying CAEServer config..."

cp $DIR/config/caeserver.properties .caebeans/

echo "Copying logger config..."

cp $DIR/config/logger.properties .caebeans/
